//
//  AddSaldoTableViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 09/06/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComunObject.h"
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface AddSaldoTableViewController : UITableViewController


@property (strong, nonatomic) IBOutlet UILabel *emailUsuario;
@property (strong, nonatomic) IBOutlet UILabel *nombreUsuario;
@property (strong, nonatomic) IBOutlet UILabel *saldoUsuario;
@property (strong, nonatomic) IBOutlet UILabel *addSaldo;

@end
