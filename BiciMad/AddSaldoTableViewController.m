//
//  AddSaldoTableViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 09/06/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "AddSaldoTableViewController.h"

@interface AddSaldoTableViewController ()

@end

@implementation AddSaldoTableViewController

bool tieneTarjetaBicimad = false;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _nombreUsuario.text = [NSString stringWithFormat:@"%@ %@ %@",
                           [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_NOMBRE],
                           [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO],
                           [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO2]];
    _emailUsuario.text = [NSString stringWithFormat:@"%@",
                           [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_EMAIL]];
    

}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self checkSaldoUsuario];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) checkSaldoUsuario
{
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    progressHUD.dimBackground = YES;
    progressHUD.labelText = @"Obteniendo saldo de la cuenta...";
    
    NSDictionary *myJson=@{TAG_DNI: [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DNI],
                           TAG_ID_AUTH: [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_ID_AUTH]};
    
    NSURL *url = [NSURL URLWithString:BaseURLString];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFJSONParameterEncoding;
    NSDictionary *params = myJson ;
    
    [httpClient postPath:url_get_datos_usuario parameters:params
                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                     // Print the response body in text
                     NSLog(@"Write to DB: Success");
                     NSError *error ;
                     NSDictionary* jsonFromData = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                     NSLog(@"Return string: %@", jsonFromData);
                     
                     NSString *success = [jsonFromData objectForKey:TAG_SUCCESS];
                     
                     if ([success integerValue] == 1) {
                         
                         tieneTarjetaBicimad = true;
                         
                         NSDictionary * usuarioAlta = [jsonFromData objectForKey:TAG_USUARIO];
                         
                         for (NSDictionary * usuario in usuarioAlta) {
                             _saldoUsuario.text = [NSString stringWithFormat:@"%@ €", [usuario objectForKey:TAG_SALDO]];
                         }
                         
                         [self.tableView reloadData];
                     } else if ([success integerValue] == 2) {
                         
                         tieneTarjetaBicimad = false;
                         
                         [self.tableView reloadData];
                         
                         _saldoUsuario.text = NSLocalizedStringFromTable(@"MENSAJE_RECOGER_TARJETA", @"messages", nil);
                         _saldoUsuario.textColor = [UIColor redColor];
                         [ComunObject alertError:@"MENSAJE_RECOGER_TARJETA_TITULO" idTexto:@"MENSAJE_RECOGER_TARJETA_MENSAJE"];
                     }
                     [progressHUD hide:YES];
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Write to DB: Fail");
                     
                     tieneTarjetaBicimad = false;
                     
                     [progressHUD hide:YES];
                 }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 2 && !tieneTarjetaBicimad) {
        return 1;
    } else {
        return [super tableView:tableView numberOfRowsInSection:section];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView
                       cellForRowAtIndexPath:indexPath];
    
    return cell;
}

@end
