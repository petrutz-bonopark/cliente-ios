//
//  AltaUsuarioViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 28/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD.h>
#import "CustomIOS7AlertView.h"
#import "ComunObject.h"
#import "MainVC.h"
#import "DateTimePicker.h"
#import "PreferenciasUsuarios.h"
#import "CompraAbonoViewController.h"

@interface AltaUsuarioTutorViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewDatosTutor;
@property (weak, nonatomic) IBOutlet UITextField *dniTutor;
@property (weak, nonatomic) IBOutlet UITextField *nombreTutor;
@property (weak, nonatomic) IBOutlet UITextField *apellido1Tutor;
@property (weak, nonatomic) IBOutlet UITextField *apellido2Tutor;

@property (weak, nonatomic) IBOutlet UIButton *buttonAltaUsuario;
@property (weak, nonatomic) IBOutlet UIButton *buttonCancelarAlta;

- (IBAction)backgroundTap:(id)sender;

@end
