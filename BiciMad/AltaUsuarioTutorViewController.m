//
//  AltaUsuarioTutorViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 04/06/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "AltaUsuarioTutorViewController.h"

@interface AltaUsuarioTutorViewController ()

@end

@implementation AltaUsuarioTutorViewController

@synthesize buttonAltaUsuario;
@synthesize buttonCancelarAlta;

NSDate *selectedDate;
CGPoint initialFramePoint;

- (void)viewDidLoad
{
    [super viewDidLoad];
    CALayer *btnEntrarLayer = [buttonCancelarAlta layer];
    [btnEntrarLayer setMasksToBounds:YES];
    [btnEntrarLayer setCornerRadius:3.0f];
    
    CALayer *btnAltaLayer = [buttonAltaUsuario layer];
    [btnAltaLayer setMasksToBounds:YES];
    [btnAltaLayer setCornerRadius:3.0f];
    
    initialFramePoint = CGPointMake(initialScrollAltaX, initialScrollAltaY);// self.scrollview.frame.origin;
    [self.scrollViewDatosTutor setContentOffset:initialFramePoint animated:YES];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    initialFramePoint = CGPointMake(initialScrollAltaX, initialScrollAltaY);
    [self.scrollViewDatosTutor setContentOffset:initialFramePoint animated:YES];
    
    [super viewWillAppear:animated];
}


- (IBAction) goToPagoAbonoConTutor:(id)sender
{
    self.dniTutor.text = [self.dniTutor.text uppercaseString];
    
    if (![ComunObject isNifValido:self.dniTutor.text]
        && ![ComunObject isCifValido:self.dniTutor.text]
        //&& ![ComunObject isNIEValido:self.dni.text]
        && ![ComunObject isTarjetaDeResidenciaValida:self.dniTutor.text]
        ) {
        [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"MENS_NIF_INVALIDO"];
        
    } else if ([_dniTutor.text length] > 0 &&
             [_nombreTutor.text length] > 0 &&
             [_apellido1Tutor.text length] > 0 &&
             [_apellido2Tutor.text length] > 0
             )
    {
        // Guardar datos del tutor en las preferencias
        [ComunObject savePreferenciasTutorUsuario:_dniTutor.text nombre:_nombreTutor.text apellido:_apellido1Tutor.text apellido2:_apellido2Tutor.text];
        
        [self performSegueWithIdentifier:@"AltaUsuarioConTutor" sender:self];
        
    } else {
        [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"MENS_RELLENAR_DATOS_USUARIO"];
    }
    
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (textField == self.dniTutor) {
        [textField resignFirstResponder];
        [self.nombreTutor becomeFirstResponder];
    } else if (textField == self.nombreTutor) {
        [textField resignFirstResponder];
        [self.apellido1Tutor becomeFirstResponder];
    } else if (textField == self.apellido1Tutor) {
        [textField resignFirstResponder];
        [self.apellido2Tutor becomeFirstResponder];
    } else if (textField == self.apellido2Tutor) {
        [textField resignFirstResponder];
        [self.scrollViewDatosTutor setContentOffset:initialFramePoint animated:YES];
    }
    return NO;
}


- (IBAction)backgroundTap:(id)sender {
    [self.view endEditing:YES];
    
    [self.scrollViewDatosTutor setContentOffset:initialFramePoint animated:YES];
}


@end
