//
//  AltaUsuarioViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 28/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD.h>
#import "CustomIOS7AlertView.h"
#import "ComunObject.h"
#import "MainVC.h"
#import "DateTimePicker.h"
#import "PreferenciasUsuarios.h"
#import "CompraAbonoViewController.h"

@interface AltaUsuarioViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UITextField *dni;
@property (weak, nonatomic) IBOutlet UITextField *nombre;
@property (weak, nonatomic) IBOutlet UITextField *apellido1;
@property (weak, nonatomic) IBOutlet UITextField *apellido2;
@property (weak, nonatomic) IBOutlet UITextField *telefono;
@property (weak, nonatomic) IBOutlet UITextField *fechaNacimiento;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *codigoPostal;
//@property (weak, nonatomic) IBOutlet UITextField *password;

@property DateTimePicker * datePicker;

@property (weak, nonatomic) IBOutlet UIButton *buttonAltaUsuario;
@property (weak, nonatomic) IBOutlet UIButton *buttonCancelarAlta;

- (IBAction)backgroundTap:(id)sender;
- (IBAction)ocultarTeclado:(id)sender;

@end
