//
//  AltaUsuarioViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 28/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "AltaUsuarioViewController.h"

@interface AltaUsuarioViewController ()

@end

@implementation AltaUsuarioViewController

@synthesize buttonAltaUsuario;
@synthesize buttonCancelarAlta;
@synthesize datePicker;

NSDate *selectedDate;
CGPoint initialFramePoint;
BOOL mostrarDatosTutor;

- (void)viewDidLoad
{
    [super viewDidLoad];
    CALayer *btnEntrarLayer = [buttonCancelarAlta layer];
    [btnEntrarLayer setMasksToBounds:YES];
    [btnEntrarLayer setCornerRadius:3.0f];
    
    CALayer *btnAltaLayer = [buttonAltaUsuario layer];
    [btnAltaLayer setMasksToBounds:YES];
    [btnAltaLayer setCornerRadius:3.0f];
 
    selectedDate = [NSDate new];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    datePicker = [[DateTimePicker alloc] initWithFrame:CGRectMake(0, screenHeight/2 - 35, screenWidth, screenHeight/2 + 35)];
    
    [datePicker addTargetForDoneButton:self action:@selector(donePressed)];
    [self.view addSubview:datePicker];
    datePicker.hidden =YES;
    [datePicker setMode:UIDatePickerModeDate];
    [datePicker.picker addTarget:self action:@selector(pickerChanged) forControlEvents:UIControlEventValueChanged];
    
    datePicker.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(gotoAbrirSesion)
                                                 name: @"callSegueAbrirSesion"
                                               object: nil];
    
    initialFramePoint = CGPointMake(initialScrollAltaX, initialScrollAltaY);// self.scrollview.frame.origin;
    [self.scrollview setContentOffset:initialFramePoint animated:YES];
    
    [self.dni becomeFirstResponder];
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

-(void)gotoAbrirSesion {
    [self performSegueWithIdentifier:@"PagoOK" sender:self];
}

- (IBAction) goToPagoAbono:(id)sender
{
    self.dni.text = [self.dni.text uppercaseString];

    if (![ComunObject isNifValido:self.dni.text]
        && ![ComunObject isCifValido:self.dni.text]
        //&& ![ComunObject isNIEValido:self.dni.text]
        && ![ComunObject isTarjetaDeResidenciaValida:self.dni.text]
        ) {
        [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"MENS_NIF_INVALIDO"];
        
    } else if (![self validateEmailWithString:self.email.text])
    {
        [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"MENSAJE_ERROR_EMAIL"];
    }
    else if ([_dni.text length] > 0 &&
               [_nombre.text length] > 0 &&
               [_apellido1.text length] > 0 &&
               [_apellido2.text length] > 0 &&
               [_fechaNacimiento.text length] > 0 &&
               [_telefono.text length] > 0 &&
               [_email.text length] > 0 &&
               [_codigoPostal.text length] > 0
               )
    {
        // Falta comprobar si el DNI ya está en el sistema
        [self comprobarDNIEnSistema];
        
    } else {
        [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"MENS_RELLENAR_DATOS_USUARIO"];
    }

}

- (void) comprobarDNIEnSistema
{
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    progressHUD.dimBackground = YES;
    progressHUD.labelText = @"Accediendo a tu cuenta...";
    
    NSDictionary *myJson=@{TAG_DNI: self.dni.text};
    
    NSURL *url = [NSURL URLWithString:BaseURLString];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFJSONParameterEncoding;
    NSDictionary *params = myJson ;
    
    [httpClient postPath:url_get_user_by_dni parameters:params
                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                     // Print the response body in text
                     NSLog(@"Write to DB: Success");
                     NSError *error ;
                     NSDictionary* jsonFromData = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                     NSLog(@"Return string: %@", jsonFromData);
                     
                     NSString *success = [jsonFromData objectForKey:TAG_SUCCESS];
                     NSString *message = [jsonFromData objectForKey:TAG_MESSAGE];
                     
                     if ([success integerValue] == 1) {
                        [ComunObject alertError:@"MENSAJE_ERROR_DNI_DUPLICADO_TITULO" idTexto:@"MENSAJE_ERROR_DNI_DUPLICADO_TEXTO"];
                     } else if ([success integerValue] == 0) {
                         // El DNI no existe en el sistema, vamos al registro
                         NSString * save = @"NO";
                         
                         // Guardar información de usuario en las preferencias de la aplicación
                         [ComunObject savePreferenciasUsuario:_dni.text nombre:_nombre.text apellido:_apellido1.text apellido2:_apellido2.text fechaNac:_fechaNacimiento.text telefono:_telefono.text email:_email.text direccion:@"" municipio: @"" provincia: @"" codPostal: _codigoPostal.text tipo:@"3" unidadesMedida:@"km" temperatura:@"C" saveSesion:save idAuth:@""];
                         
                         if ([ComunObject isUsuarioMenorDe:16 birthday:selectedDate]) {
                            [self performSegueWithIdentifier:@"goToAltaTutor" sender:self];
                            [PreferenciasUsuarios saveBoolInPreferences:PREFERENCIA_USUARIO_CON_TUTOR value:true];
                         } else {
                            [self performSegueWithIdentifier:@"AltaUsuario" sender:self];
                            [PreferenciasUsuarios saveBoolInPreferences:PREFERENCIA_USUARIO_CON_TUTOR value:false];
                         }
                         
                     } else {
                         [ComunObject alertErrorMessage:@"TITULO_ALERT_ERROR" texto:message];
                     }
                     // Ocultamos el progressHUD
                     [progressHUD hide:YES];
                     
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Write to DB: Fail");
                     [progressHUD hide:YES];
                     
                     [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"LABEL_ERROR_CONNECTION"];
                 }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    if (textField == self.dni) {
        [textField resignFirstResponder];
        [self.nombre becomeFirstResponder];
    } else if (textField == self.nombre) {
        [textField resignFirstResponder];
        [self.apellido1 becomeFirstResponder];
    } else if (textField == self.apellido1) {
        [textField resignFirstResponder];
        [self.apellido2 becomeFirstResponder];
    } else if (textField == self.apellido2) {
        [textField resignFirstResponder];
        [self.fechaNacimiento becomeFirstResponder];
    } else if (textField == self.fechaNacimiento) {
        [textField resignFirstResponder];
        [self.telefono becomeFirstResponder];
    } else if (textField == self.telefono) {
        [textField resignFirstResponder];
        [self.email becomeFirstResponder];
    } else if (textField == self.email) {
        [textField resignFirstResponder];
        [self.codigoPostal becomeFirstResponder];
    } else if (textField == self.codigoPostal) {
        [textField resignFirstResponder];
        [self.scrollview setContentOffset:initialFramePoint animated:YES];
    }
    
//    NSInteger nextTag = textField.tag + 1;
//    // Try to find next responder
//    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
//    
//    if (!nextResponder) {
//        [self.scrollview setContentOffset:initialFramePoint animated:YES];
//        [textField resignFirstResponder];
//        return YES;
//    }
    return NO;
}


- (IBAction)backgroundTap:(id)sender {
    [self.view endEditing:YES];
    datePicker.hidden = YES;
    
    [self.scrollview setContentOffset:initialFramePoint animated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.fechaNacimiento) {
        [textField resignFirstResponder];
        datePicker.hidden = NO;
    }
    
    //activeField = textField;
    [self.scrollview setContentOffset:CGPointMake(0, [self.scrollview convertPoint:CGPointZero fromView:textField].y - valorSubidaTextField) animated:YES];
}

- (IBAction)ocultarDatePicker:(id)sender {

    datePicker.hidden = YES;
}

- (IBAction)ocultarTeclado:(id)sender {
    
    UITextField * field = (UITextField *) sender;
    
    [self textFieldShouldReturn:field];
}


-(void)pickerChanged {
    selectedDate = datePicker.picker.date;
}

-(void)donePressed {
    datePicker.hidden = YES;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    if ([ComunObject isUsuarioMayorDe:14 birthday:selectedDate]) {
        _fechaNacimiento.text = [dateFormatter stringFromDate:selectedDate];
    } else {
        [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"MENSAJE_EDAD_NO_PERMITIDA"];
    }
//    [self.scrollview setContentOffset:initialFramePoint animated:YES];
}

-(void)buttonPressed:(id)sender {
    datePicker.hidden = YES;
    [datePicker.picker setDate:selectedDate];
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

@end
