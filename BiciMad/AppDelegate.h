//
//  AppDelegate.h
//  BiciMad
//
//  Created by Miguel Asuar on 31/01/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <AFNetworking/AFNetworking.h>

//BOOL isShowingNotification = false;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
