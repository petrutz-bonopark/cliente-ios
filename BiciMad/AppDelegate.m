//
//  AppDelegate.m
//  BiciMad
//
//  Created by Miguel Asuar on 31/01/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "AppDelegate.h"
#import "ComunObject.h"


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
 
    // Register for push notifications
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    // Handle launching from a notification
    UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (locationNotification) {
        // Set icon badge number to zero
        application.applicationIconBadgeNumber = 0;
    }
    
    // Override point for customization after application launch.
    
    [GMSServices provideAPIKey:@"AIzaSyBP2KUPhN621FTLAmgV4ONLquXmZYsZrFs"];
    
    // Cambiar el color de la Navigation Bar
    [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(ColorApp)];
    // Cambiar el color del Back button
    // [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    // Cambiar el tipo de letra en la Navigation Bar
    NSShadow  *shadow = [[NSShadow  alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 1);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary  dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                           shadow, NSShadowAttributeName,
                                                           [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:22.0], NSFontAttributeName, nil]];
    

    // Cambiar el color de los dots del page controller para diferenciar la página actual
//    UIPageControl *pageControl = [UIPageControl appearance];
//    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
//    pageControl.currentPageIndicatorTintColor = [UIColor orangeColor];
//    pageControl.backgroundColor = [UIColor clearColor];
    
//    Cambiar el color del texto de la status bar
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // Mantener la pantalla activa
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
//    [self scheduleNotificationWithItem:Nil interval:60];
//    [self cancelAllNotifications];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    NSLog(@"Application entered background state.");
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)notif
//{
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
//    
////    if (!isShowingNotification) {
////        [self checkSaldoUsuario:notif];
////    }
//    // app.applicationIconBadgeNumber = notif.applicationIconBadgeNumber + 1;
//    
//    //notif.soundName = UILocalNotificationDefaultSoundName;
//     
//}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"Remote notification received!!");
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    if (![deviceToken isEqualToData:NULL]) {
        NSLog(@"My token is: %@", deviceToken);
        const unsigned *tokenBytes = [deviceToken bytes];
    
        NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                          ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                          ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                          ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    
        [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_DEV_TOKEN value:hexToken];
    }
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}

- (void) cancelAllNotifications
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

@end
