//
//  TCMapViewController.h
//  TCGoogleMaps
//
//  Created by Lee Tze Cheun on 8/17/13.
//  Copyright (c) 2013 Lee Tze Cheun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD.h>

#import "BMStepsDelegate.h"
#import "CustomIOS7AlertView.h"
#import "ComunObject.h"

/**
 * View Controller to display the Google Maps view.
 */
@interface BMMapViewController : UIViewController <BMStepsDelegate>


@property (nonatomic, weak) NSDictionary *estaciones;
@property (nonatomic, strong) NSMutableArray *estacionesCercanas;
/**
 * Marks the user's current location on the map view and get the place's details
 * with the given place reference string.
 *
 * @param myLocation     the user's current location
 * @param placeReference the place reference string to retrieve the place's details
 */
- (void)setMyLocation:(CLLocation *)myLocation placeReference:(NSString *)placeReference;

@end
