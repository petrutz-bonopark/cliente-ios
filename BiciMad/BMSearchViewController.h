//
//  TCSearchViewController.h
//  TCGoogleMaps
//
//  Created by Lee Tze Cheun on 8/19/13.
//  Copyright (c) 2013 Lee Tze Cheun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD.h>

#import "BMStepsDelegate.h"
#import "CustomIOS7AlertView.h"
#import "ComunObject.h"

/*
 View Controller to search for nearby places using Google Places API.
 */
@interface BMSearchViewController : UIViewController
    <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, BMStepsDelegate>

@property (nonatomic, weak) NSDictionary *estaciones;
@property (nonatomic, strong) NSMutableArray *estacionesCercanas;
/**
 * Marks the user's current location on the map view and get the place's details
 * with the given place reference string.
 *
 * @param myLocation     the user's current location
 * @param placeReference the place reference string to retrieve the place's details
 */
@property (retain, nonatomic) IBOutlet UIBarButtonItem *barItemSearch;
- (void)setMyLocation:(CLLocation *)myLocation placeReference:(NSString *)placeReference;
- (IBAction)searchPlaces:(id)sender;

- (IBAction)backgroundTap;
@end
