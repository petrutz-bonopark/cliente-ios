//
//  TCSearchViewController.m
//  TCGoogleMaps
//
//  Created by Lee Tze Cheun on 8/19/13.
//  Copyright (c) 2013 Lee Tze Cheun. All rights reserved.
//

#import "BMSearchViewController.h"
#import "BMMapViewController.h"
#import "TCUserLocationManager.h"
#import "TCGooglePlaces.h"

#import <MBProgressHUD/MBProgressHUD.h>

@interface BMSearchViewController ()

@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *placePredictions;

@property (nonatomic, strong, readonly) TCUserLocationManager *userLocationManager;
@property (nonatomic, strong) CLLocation *myLocation;

/** Google Maps view. */
@property (nonatomic, weak) IBOutlet GMSMapView *mapView;

/**
 * Labels to display the route's name, distance and duration.
 */
@property (nonatomic, weak) IBOutlet UIView *routeDetailsView;
@property (nonatomic, weak) IBOutlet UILabel *routeNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *distanceAndDurationLabel;

/** The bar button item to view detail steps of the route. */
@property (nonatomic, weak) IBOutlet UIBarButtonItem *stepsBarButtonItem;

/**
 * A unique token that you can use to retrieve additional information
 * about this place in a Place Details request.
 */
@property (nonatomic, copy, readonly) NSString *placeReference;

/** Place Details result returned from Google Places API. */
@property (nonatomic, strong) TCPlace *place;

/** Route result returned from Google Directions API. */
@property (nonatomic, strong) TCDirectionsRoute *route;

/** The marker for the step's location. */
@property (nonatomic, strong) GMSMarker *stepMarker;

/** The marker that represents the destination. */
@property (nonatomic, strong) GMSMarker *destinationMarker;
@end

// Google Places Autocomplete API uses the radius to determine the area to search places in.
static CLLocationDistance const kSearchRadiusInMeters = 15000.0f;

@implementation BMSearchViewController

@synthesize userLocationManager = _userLocationManager;

@synthesize estaciones;
@synthesize estacionesCercanas;

UIButton *menuButton;

CustomIOS7AlertView *alertView;

#pragma mark - Models

- (void)setMyLocation:(CLLocation *)myLocation placeReference:(NSString *)aPlaceReference
{
    // Update my location, only if it has changed.
    if (_myLocation != myLocation) {
        _myLocation = [myLocation copy];
    }
    
    // Only fetch new place details from Google Places API, if place's
    // reference has changed.
    if (_placeReference != aPlaceReference) {
        _placeReference = [aPlaceReference copy];
        
        // Hide the steps bar button item, until we have a valid route.
        self.navigationItem.rightBarButtonItem = nil;
        
        [self getPlaceDetailsWithReference:_placeReference];
    }
    
}

- (IBAction)searchPlaces:(id)sender
{
    self.navigationItem.titleView = self.searchBar;
    [self.barItemSearch setAction:@selector(cancelSearchPlace)];
    self.barItemSearch.style = UIBarButtonSystemItemCancel;
}

- (IBAction)cancelSearchPlace
{
    self.navigationItem.titleView = nil;
    [self.barItemSearch setAction:@selector(searchPlaces:)];
    self.barItemSearch.style = UIBarButtonSystemItemSearch;
    
    [self backgroundTap];
}

#pragma mark - View Events

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Add the search bar to the navigation bar.
    // self.navigationItem.titleView = self.searchBar;
    
    self.estacionesCercanas = [[NSMutableArray alloc] init];
    // Tell Google Maps to draw the user's location on the map view.
    self.mapView.myLocationEnabled = YES;
    self.mapView.settings.myLocationButton = YES;
    
    //UIEdgeInsets mapInsets = UIEdgeInsetsMake(0.0, 0.0, 20.0, 0.0);
    //self.mapView.padding = mapInsets;
    // [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    // [self addMenuButton];
    
    [self getEstaciones];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // If we have not located the user yet, we should find where the user is.
    // The results returned from the autocomplete is based on the user's location.
    if (!self.myLocation) {
        [self startLocatingUser];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Stop location services when this view disappears to save power consumption.
    [self.userLocationManager stopLocatingUser];
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    self.placePredictions = nil;
}

#pragma mark - UISearchBar Delegate

/**
 * While user types in the search field, we will asynchronously fetch a list
 * of place suggestions.
 */
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    // If user deleted the search text, we should not waste resources sending an
    // empty input string to Google Places Autocomplete API.
    if (nil == searchText || 0 == [searchText length]) {
        self.placePredictions = nil;
        [self.tableView reloadData];
        return;
    }
    
    // Request parameters to be send to Google Places Autocomplete API.
    TCPlacesAutocompleteParameters *parameters = [[TCPlacesAutocompleteParameters alloc] init];
    parameters.input = searchText;
    parameters.location = self.myLocation.coordinate;
    parameters.radius = kSearchRadiusInMeters;
    
    [[TCPlacesService sharedService] placePredictionsWithParameters:parameters completion:^(NSArray *predictions, NSError *error) {
        // It is possible that the UISearchBar's text has changed when we return
        // from the network with the results. If it has changed, we should not
        // display stale results on the table view.
        if (predictions && [parameters.input isEqualToString:searchBar.text]) {
            self.placePredictions = predictions;
            [self.tableView reloadData];
        } else {
            // Google Places Autocomplete API error handling.
            if (NSURLErrorCancelled == error.code) {
                NSLog(@"[Google Places Autocomplete API] - Cancelled request for input \"%@\".", parameters.input);
            } else {
                NSString *statusCode = error.userInfo[TCPlacesServiceStatusCodeErrorKey];
                NSString *description = [error localizedDescription];
                
                if (statusCode) {
                    NSLog(@"[Google Places Autocomplete API] - Status Code: %@, Error: %@", statusCode, description);
                } else {
                    NSLog(@"[Google Places Autocomplete API] - Error: %@", description);
                }
            }
        }                
    }];        
}

#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.placePredictions ? self.placePredictions.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * const CellIdentifier = @"SearchResultCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    TCPlacesAutocompletePrediction *prediction = self.placePredictions[indexPath.row];
    
    // First prediction term will be the name of the place.
    TCPlacesPredictionTerm *firstTerm = prediction.terms[0];
    cell.textLabel.text = firstTerm.value;

    // Remaining terms will be the address of the place.
    NSArray *remainingTerms = [prediction.terms subarrayWithRange:NSMakeRange(1, prediction.terms.count - 1)];
    cell.detailTextLabel.text = [remainingTerms componentsJoinedByString:@", "];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TCPlacesAutocompletePrediction *prediction = self.placePredictions[indexPath.row];
    
    [self setMyLocation:self.myLocation placeReference:prediction.reference];
    
    [self cancelSearchPlace];
}

#pragma mark - Storyboard

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowMap"]) {
        // Get the selected place.
        NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
        TCPlacesAutocompletePrediction *prediction = self.placePredictions[selectedIndexPath.row];
        
        // Display it on the map with directions.
        BMMapViewController *mapViewController = (BMMapViewController *) [segue destinationViewController];
        [mapViewController setMyLocation:self.myLocation
                          placeReference:prediction.reference];
    }
}

#pragma mark - User Location Manager

- (TCUserLocationManager *)userLocationManager
{
    if (!_userLocationManager) {
        _userLocationManager = [[TCUserLocationManager alloc] init];
    }
    return _userLocationManager;
}

- (void)startLocatingUser
{
    // Show progress HUD while we find the user's location.
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    progressHUD.dimBackground = YES;
    progressHUD.labelText = @"Buscando su posición";
    
    // Make progress HUD consume all touches to disable interaction on
    // background views.
    progressHUD.userInteractionEnabled = YES;
    
    // Get the user's current location. Google Places API uses the user's
    // current location to find relevant places.
    [self.userLocationManager startLocatingUserWithCompletion:^(CLLocation *userLocation, NSError *error) {
        if (userLocation) {
            self.myLocation = userLocation;
            [_mapView animateWithCameraUpdate:
             [GMSCameraUpdate setTarget:userLocation.coordinate zoom:15.0f]];
            [progressHUD hide:YES];
            
            // Set focus to the UISearchBar, so that user can start
            // entering their query right away.
            [self.searchBar becomeFirstResponder];
        } else {
            NSLog(@"[TCUserLocationManager] - Error: %@", [error localizedDescription]);
        }
    }];    
}

-(BOOL)checkString:(NSString *)originalString  contains :(NSString *)subString{
    BOOL found = NO;
    if ([originalString rangeOfString:subString].location != NSNotFound) {
        found = YES;
        
    }
    return found;
}

#pragma mark - Google Places API

- (void)getPlaceDetailsWithReference:(NSString *)reference
{
    [[TCPlacesService sharedService] placeDetailsWithReference:reference completion:^(TCPlace *place, NSError *error) {
        if (place) {
            self.place = place;
            
            // Buscar la estación más cercana al destino elegido.
            [self getEstacionesCercanas:place];
            
            
            //            // Create marker for the destination on the map view.
            //            self.destinationMarker = [self createMarkerForPlace:self.place onMap:self.mapView];
            //
            //            // Focus camera on destination.
            //            [self.mapView animateWithCameraUpdate:
            //             [GMSCameraUpdate setTarget:self.destinationMarker.position]];
            //
            //            // Request Google Directions API for directions starting from
            //            // my location to destination.
            //            if (self.myLocation) {
            //                [self getDirectionsFromMyLocation:self.myLocation
            //                                          toPlace:self.place];
            //            }
        } else {
            NSLog(@"[Google Place Details API] - Error : %@", [error localizedDescription]);
        }
    }];
}

- (GMSMarker *)createMarkerForPlace:(TCPlace *)place onMap:(GMSMapView *)mapView
{
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = place.location;
    marker.title = place.name;
    marker.snippet = place.address;
    marker.map = mapView;
    
    return marker;
}

#pragma mark - Google Directions API

- (void)getDirectionsFromMyLocation:(CLLocation *)myLocation toPlace:(TCPlace *)place
{
    // Configure the parameters to be send to TCDirectionsService.
    TCDirectionsParameters *parameters = [[TCDirectionsParameters alloc] init];
    parameters.origin = self.myLocation.coordinate;
    parameters.destination = place.location;
    
    [[TCDirectionsService sharedService] routeWithParameters:parameters completion:^(NSArray *routes, NSError *error) {
        if (routes) {
            // There should only be one route since we did not ask for alternative routes.
            self.route = routes[0];
            
            // Move camera viewport to fit the viewport bounding box of this route.
            [self.mapView animateWithCameraUpdate:
             [GMSCameraUpdate fitBounds:self.route.bounds]];
            
            [ComunObject drawRoute:self.route onMap:self.mapView];
            [self showRouteDetailsViewWithRoute:self.route];
            
            // With a valid route, we can now allow user to view the step-by-step instructions.
            self.navigationItem.rightBarButtonItem = self.stepsBarButtonItem;
        } else {
            NSLog(@"[Google Directions API] - Error: %@", [error localizedDescription]);
        }
    }];
}

- (void)getDirectionsFromMyLocation:(CLLocation *)myLocation toEstacion:(TCEstacion *)estacion
{
    // Configure the parameters to be send to TCDirectionsService.
    TCDirectionsParameters *parameters = [[TCDirectionsParameters alloc] init];
    parameters.origin = self.myLocation.coordinate;
    parameters.destination = estacion.location;
    
    [[TCDirectionsService sharedService] routeWithParameters:parameters completion:^(NSArray *routes, NSError *error) {
        if (routes) {
            // There should only be one route since we did not ask for alternative routes.
            self.route = routes[0];
            
            // Move camera viewport to fit the viewport bounding box of this route.
            [self.mapView animateWithCameraUpdate:
             [GMSCameraUpdate fitBounds:self.route.bounds]];
            
            [ComunObject drawRoute:self.route onMap:self.mapView];
            [self showRouteDetailsViewWithRoute:self.route];
            
            // With a valid route, we can now allow user to view the step-by-step instructions.
            self.navigationItem.rightBarButtonItem = self.stepsBarButtonItem;
        } else {
            NSLog(@"[Google Directions API] - Error: %@", [error localizedDescription]);
        }
    }];
}

- (void)showRouteDetailsViewWithRoute:(TCDirectionsRoute *)route
{
    self.routeNameLabel.text = route.summary;
    
    // With no waypoints, we only have one leg.
    TCDirectionsLeg *leg = route.legs[0];
    self.distanceAndDurationLabel.text = [NSString stringWithFormat:@"%@, %@",
                                          leg.distance.text, leg.duration.text];
    
    // Fade in animation for the route details view.
    self.routeDetailsView.alpha = 0.0f;
    [UIView animateWithDuration:1.0f animations:^{
        self.routeDetailsView.alpha = 1.0f;
    }];
}

#pragma mark - TCStepsViewController Delegate

- (void)stepsViewControllerDidSelectMyLocation:(TCStepsViewController *)stepsViewController
{
    // Passing in nil to selectMarker will deselect any currently selected marker.
    [self mapView:self.mapView setCameraTarget:self.myLocation.coordinate selectMarker:nil];
}

- (void)stepsViewControllerDidSelectDestination:(TCStepsViewController *)stepsViewController
{
    [self mapView:self.mapView setCameraTarget:self.destinationMarker.position selectMarker:self.destinationMarker];
}

- (void)stepsViewController:(TCStepsViewController *)stepsViewController didSelectStep:(TCDirectionsStep *)step
{
    // Zoom in to fit the step's path.
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:step.path];
    [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
    
    // Remove any previous step's marker from the map.
    self.stepMarker.map = nil;
    
    // Create marker to represent the start of the step.
    self.stepMarker = [self createMarkerForStep:step onMap:self.mapView];
    
    // Select the step marker to show its info window.
    self.mapView.selectedMarker = self.stepMarker;
    [self.mapView animateToLocation:self.stepMarker.position];
}

- (GMSMarker *)createMarkerForStep:(TCDirectionsStep *)step onMap:(GMSMapView *)mapView
{
    GMSMarker *marker = [GMSMarker markerWithPosition:step.startLocation];
    marker.icon = [self stepMarkerIcon];
    marker.snippet = step.instructions;
    marker.map = self.mapView;
    
    return marker;
}

/**
 * Returns the image used for the selected step's marker icon.
 */
- (UIImage *)stepMarkerIcon
{
    // Here we are just creating a 1x1 transparent image to be used for
    // the marker icon. Thus, making the marker icon invisible.
    static UIImage * _image = nil;
    if (!_image) {
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(1.0f, 1.0f), NO, 0.0f);
        _image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return _image;
}

/**
 * Zooms the camera in at given coordinate and selects the marker to open
 * its info window.
 *
 * @param mapView    The GMSMapView instance.
 * @param coordinate The coordinate to focus camera on.
 * @param marker     The marker to select on the mapView.
 */
- (void)mapView:(GMSMapView *)mapView setCameraTarget:(CLLocationCoordinate2D)coordinate selectMarker:(GMSMarker *)marker
{
    // Show the info window of the selected marker.
    mapView.selectedMarker = marker;
    
    // Zoom in to focus on target location.
    [mapView animateWithCameraUpdate:
     [GMSCameraUpdate setTarget:coordinate zoom:17.0f]];
}


- (void)getEstaciones
{
    // Mostrar progressHUD mientras se registra la ruta en BD
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    progressHUD.dimBackground = YES;
    progressHUD.labelText = @"Obteniendo estado de las estaciones";
    
    // 1
    NSString  *estacionesUrl = [NSString  stringWithFormat:@"%@%@?format=json", BaseURLString, url_all_estaciones];
    NSURL  *url = [NSURL  URLWithString:estacionesUrl];
    NSURLRequest  *request = [NSURLRequest  requestWithURL:url];
    
    // 2
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
     // 3
                                                    success:^(NSURLRequest  *request, NSHTTPURLResponse  *response, id JSON) {
                                                        self.estaciones  = (NSDictionary  *)JSON;
                                                        [self showAllEstaciones];
                                                        
                                                        [progressHUD hide:YES];
                                                        //         self.title = @"JSON Retrieved";
                                                    }
     // 4
                                                    failure:^(NSURLRequest  *request, NSHTTPURLResponse  *response, NSError  *error, id JSON) {
                                                        [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"LABEL_ERROR_CONNECTION"];
                                                        [progressHUD hide:YES];
                                                    }];
    
    // 5
    [operation start];
}


- (void) showAllEstaciones
{
    NSDictionary *estacionesMadrid = [self.estaciones objectForKey:@"estaciones"];
    
    for (NSDictionary *estacion in estacionesMadrid){
        NSString* nombre = [estacion  objectForKey:@"nombre"];
        NSString* direccion = [estacion  objectForKey:@"direccion"];
        NSString* latitud = [estacion  objectForKey:@"latitud"];
        NSString* longitud = [estacion  objectForKey:@"longitud"];
        NSString* activo = [estacion  objectForKey:@"activo"];
        
        CLLocationDegrees lat = [latitud floatValue];
        CLLocationDegrees lon = [longitud floatValue];
        
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake(lat, lon);
        
        TCEstacion *estacionPlace = [TCEstacion alloc];
        estacionPlace.nombre = nombre;
        estacionPlace.direccion = direccion;
        estacionPlace.location = location;
        estacionPlace.activo = activo;
        
        [self createMarkerForEstacion:estacionPlace onMap:self.mapView];
    }
}

- (GMSMarker *)createMarkerForEstacion:(TCEstacion *)estacion onMap:(GMSMapView *)mapView
{
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = estacion.location;
    marker.title = estacion.nombre;
    marker.snippet = estacion.direccion;
    marker.map = mapView;
    
    switch ([estacion.luz integerValue]) {
        case 0:
            marker.icon = [UIImage imageNamed:@"estacion_libre"];
            break;
        case 1:
            marker.icon = [UIImage imageNamed:@"estacion_ocupada"];
            break;
        default:
            marker.icon = [UIImage imageNamed:@"estacion_ocupada"];
            break;
    }
    
    return marker;
}

- (void)getEstacionesCercanas:(TCPlace *) destino
{
    NSString  *estacionesUrl = [NSString  stringWithFormat:@"%@%@?format=json&latitud=%f&longitud=%f", BaseURLString, url_estaciones_cercanas, destino.location.latitude, destino.location.longitude];
    NSURL  *url = [NSURL  URLWithString:estacionesUrl];
    NSURLRequest  *request = [NSURLRequest  requestWithURL:url];
    
    // 2
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
     // 3
                                                    success:^(NSURLRequest  *request, NSHTTPURLResponse  *response, id JSON) {
                                                        self.estaciones  = (NSDictionary  *)JSON;
                                                        [self alertSeleccionEstacion];
                                                    }
     // 4
                                                    failure:^(NSURLRequest  *request, NSHTTPURLResponse  *response, NSError  *error, id JSON) {
                                                        [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"LABEL_ERROR_CONNECTION"];
                                                    }];
    
    // 5
    [operation start];
}

- (void) alertSeleccionEstacion
{
    alertView = [[CustomIOS7AlertView alloc] init];
    
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedStringFromTable(@"LABEL_CLOSE", @"messages", nil), nil]];
    [alertView setUseMotionEffects:TRUE];
    [alertView setButtonTitles:NULL];
    [alertView setContainerView:[self crearBotonesEstaciones]];
    
    [alertView show];
    
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Button at position %ld is clicked on alertView %ld.", (long)buttonIndex, (long)[alertView tag]);
}

- (UIView *)crearBotonesEstaciones
{
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 200)];
    
    NSDictionary *estacionesMadrid = [self.estaciones objectForKey:@"estaciones"];
    
    UILabel *estacionesLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, 250, 20)];
    estacionesLabel.backgroundColor = [UIColor clearColor];
    estacionesLabel.textColor=[UIColor whiteColor];
    [estacionesLabel setFont: [UIFont fontWithName:@"HelveticaNeue" size:11.0f]];
    estacionesLabel.text = NSLocalizedStringFromTable(@"LABEL_ESTACIONES_CERCANAS", @"messages", nil);
    [demoView addSubview:estacionesLabel];
    
    int index = 0;
    for (NSDictionary *estacion in estacionesMadrid){
        NSString* nombre = [estacion  objectForKey:@"nombre"];
        NSString* direccion = [estacion  objectForKey:@"direccion"];
        NSString* latitud = [estacion  objectForKey:@"latitud"];
        NSString* longitud = [estacion  objectForKey:@"longitud"];
        NSString* activo = [estacion  objectForKey:@"activo"];
        
        CLLocationDegrees lat = [latitud floatValue];
        CLLocationDegrees lon = [longitud floatValue];
        
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake(lat, lon);
        
        TCEstacion *estacionPlace = [TCEstacion alloc];
        estacionPlace.nombre = nombre;
        estacionPlace.direccion = direccion;
        estacionPlace.location = location;
        estacionPlace.activo = activo;
        
        [self.estacionesCercanas addObject:estacionPlace];
        
        UIButton *botonEstacion = [UIButton buttonWithType:UIButtonTypeSystem];
        botonEstacion.frame = CGRectMake(25.0f, 100.0f * index + 50.0f, 240.0f, 37.0f);
        [botonEstacion setTitle:nombre forState:UIControlStateNormal];
        botonEstacion.tintColor = [UIColor whiteColor];
        
        botonEstacion.tag = index;
        [botonEstacion addTarget:self action:@selector(createRouteToEstacion:)
                forControlEvents:UIControlEventTouchDown];
        
        [demoView addSubview:botonEstacion];
        index++;
    }
    
    //    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 270, 180)];
    //    [imageView setImage:[UIImage imageNamed:@"demo"]];
    //    [demoView addSubview:imageView];
    
    return demoView;
}

- (void) createRouteToEstacion: (id) sender
{
    UIButton *botonPulsado = (UIButton *) sender;
    int index = (int) botonPulsado.tag;
    
    TCEstacion *estacion = [self.estacionesCercanas objectAtIndex:index];
    // Create marker for the destination on the map view.
    self.destinationMarker = [self createMarkerForPlace:self.place onMap:self.mapView];
    //
    // Focus camera on destination.
    [self.mapView animateWithCameraUpdate:
     [GMSCameraUpdate setTarget:self.destinationMarker.position]];
    //
    // Request Google Directions API for directions starting from
    // my location to destination.
    if (self.myLocation) {
        [self getDirectionsFromMyLocation:self.myLocation
                               toEstacion:estacion];
    }
    
    [alertView close];
    
}

- (IBAction)backgroundTap
{
    [self.searchBar resignFirstResponder];
}

//- (void)addMenuButton
//{
//    
//    menuButton = [[UIButton alloc] initWithFrame: CGRectMake(5.0f, 50.0f, 30.0f, 30.0f)];
//    UIImage *backImage = [UIImage imageNamed:@"menu_show"];
//    [menuButton setBackgroundImage:backImage  forState:UIControlStateNormal];
//    //[backButton setTitle:@"Back" forState:UIControlStateNormal];
//    [menuButton addTarget:self action:@selector(showNavigationBar) forControlEvents:UIControlEventTouchUpInside];
//    [self.mapView addSubview:menuButton];
//}
//
//
//-(void) showNavigationBar {
//    [self.navigationController setNavigationBarHidden:NO animated:YES];
//    //menuButton = [[UIButton alloc] initWithFrame: CGRectMake(5, 30, 30.0f, 30.0f)];
//    UIImage *backImage = [UIImage imageNamed:@"menu_hide"];
//    [menuButton setBackgroundImage:backImage  forState:UIControlStateNormal];
//    [menuButton addTarget:self action:@selector(hideNavigationBar) forControlEvents:UIControlEventTouchUpInside];
//    
//    UIEdgeInsets mapInsets = UIEdgeInsetsMake(0.0f, 0.0f, 30.0f, 0.0f);
//    self.mapView.padding = mapInsets;
//    
//}
//
//-(void) hideNavigationBar {
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
//    //menuButton = [[UIButton alloc] initWithFrame: CGRectMake(5, 20, 30.0f, 30.0f)];
//    UIImage *backImage = [UIImage imageNamed:@"menu_show"];
//    [menuButton setBackgroundImage:backImage  forState:UIControlStateNormal];
//    [menuButton addTarget:self action:@selector(showNavigationBar) forControlEvents:UIControlEventTouchUpInside];
//    
//    UIEdgeInsets mapInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
//    self.mapView.padding = mapInsets;
//}

@end
