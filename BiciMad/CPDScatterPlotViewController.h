//
//  CPDScatterPlotViewController.h
//  CorePlotDemo
//
//  Created by Fahim Farook on 19/5/12.
//  Copyright 2012 RookSoft Pte. Ltd. All rights reserved.
//

#import "ComunObject.h"

@interface CPDScatterPlotViewController : UIViewController <CPTPlotDataSource>

@property (nonatomic, strong) IBOutlet CPTGraphHostingView *hostView;

@property NSUInteger pageIndex;
@property (weak, nonatomic) IBOutlet UILabel *altitudMax;
@property (weak, nonatomic) IBOutlet UILabel *altitudMin;

@property NSDictionary *coordenadas;

@property NSMutableArray *altitudesRuta;
@property NSMutableArray *tiempoRuta;

- (void)setMyCoordenadas: (NSDictionary *) coordenadas;
@end
