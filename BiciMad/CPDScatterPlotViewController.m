//
//  CPDScatterPlotViewController.m
//  CorePlotDemo
//
//  Created by Fahim Farook on 19/5/12.
//  Copyright 2012 RookSoft Pte. Ltd. All rights reserved.
//

#import "CPDScatterPlotViewController.h"

@implementation CPDScatterPlotViewController

@synthesize hostView = hostView_;

CGFloat altitudMax = 0.0f;
CGFloat altitudMin = 0.0f;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (void)setMyCoordenadas: (NSDictionary *) coordenadas
{
    _coordenadas = [coordenadas copy];
}

- (void)getDirectionsFromRoute
{
    altitudMin = 0.0f;
    altitudMax = 0.0f;
    
    self.altitudesRuta = [[NSMutableArray alloc] init];
    self.tiempoRuta = [[NSMutableArray alloc] init];
    
    NSArray *arr = [NSJSONSerialization JSONObjectWithData:[[NSString stringWithFormat:@"%@", _coordenadas] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];
    
    for (NSDictionary * coordenada in arr) {
        NSString *altitude = [coordenada objectForKey:TAG_COORDENADAS_ALTITUDE];
        NSString *timestamp = [coordenada objectForKey:TAG_COORDENADAS_TIMESTAMP];
        
        if ([altitude floatValue] > altitudMax) {
            altitudMax = [altitude floatValue];
        }
        
        if (altitudMin == 0 || [altitude floatValue] < altitudMin) {
            altitudMin = [altitude floatValue];
        }
        
        [self.altitudesRuta addObject:altitude];
        [self.tiempoRuta addObject:[ComunObject formatDateFromString:timestamp pattern:@"HH:mm:ss"]];
    }
    
    self.altitudMax.text = [NSString stringWithFormat:@"Altitud máxima: %.02f m", altitudMax];
    self.altitudMin.text = [NSString stringWithFormat:@"Altitud mínima: %.02f m", altitudMin];
}

#pragma mark - UIViewController lifecycle methods
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self getDirectionsFromRoute];
    [self initPlot];
}

#pragma mark - Chart behavior
-(void)initPlot {
    //[self configureHost];
    [self configureGraph];
    [self configurePlots];
    [self configureAxes];    
}

-(void)configureHost {  
	self.hostView = [(CPTGraphHostingView *) [CPTGraphHostingView alloc] initWithFrame:self.view.bounds];
	self.hostView.allowPinchScaling = YES;    
	[self.view addSubview:self.hostView];    
}

-(void)configureGraph {
	// 1 - Create the graph
	CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostView.bounds];
	//[graph applyTheme:[CPTTheme themeNamed:kCPTPlainWhiteTheme]];
	self.hostView.hostedGraph = graph;
	// 2 - Set graph title
	NSString *title = @"Tabla de altimetría";
	graph.title = title;  
	// 3 - Create and set text style
	CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
	titleStyle.color = [CPTColor blueColor];
	titleStyle.fontName = @"Helvetica";
	titleStyle.fontSize = 11.0f;
	graph.titleTextStyle = titleStyle;
	graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
	graph.titleDisplacement = CGPointMake(0.0f, 10.0f);
	// 4 - Set padding for plot area
	[graph.plotAreaFrame setPaddingLeft:30.0f];    
	[graph.plotAreaFrame setPaddingBottom:10.0f];
	// 5 - Enable user interactions for plot space
	CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
	plotSpace.allowsUserInteraction = YES; 
}

-(void)configurePlots { 
	// 1 - Get graph and plot space
	CPTGraph *graph = self.hostView.hostedGraph;
	CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
	// 2 - Create the three plots
	CPTScatterPlot *aaplPlot = [[CPTScatterPlot alloc] init];
	aaplPlot.dataSource = self;
	aaplPlot.identifier = CPDTickerSymbolAAPL;
	CPTColor *aaplColor = [CPTColor redColor];
	[graph addPlot:aaplPlot toPlotSpace:plotSpace];    
//	CPTScatterPlot *googPlot = [[CPTScatterPlot alloc] init];
//	googPlot.dataSource = self;
//	googPlot.identifier = CPDTickerSymbolGOOG;
//	CPTColor *googColor = [CPTColor greenColor];
//	[graph addPlot:googPlot toPlotSpace:plotSpace];    
//	CPTScatterPlot *msftPlot = [[CPTScatterPlot alloc] init];
//	msftPlot.dataSource = self;
//	msftPlot.identifier = CPDTickerSymbolMSFT;
//	CPTColor *msftColor = [CPTColor blueColor];
//	[graph addPlot:msftPlot toPlotSpace:plotSpace];  
	// 3 - Set up plot space
	[plotSpace scaleToFitPlots:[NSArray arrayWithObjects:aaplPlot, nil]];
    CPTMutablePlotRange *xRange = [plotSpace.xRange mutableCopy];
	[xRange expandRangeByFactor:CPTDecimalFromCGFloat(1.1f)];        
	plotSpace.xRange = xRange;
	CPTMutablePlotRange *yRange = [plotSpace.yRange mutableCopy];
	[yRange expandRangeByFactor:CPTDecimalFromCGFloat(1.2f)];        
	plotSpace.yRange = yRange;    
	// 4 - Create styles and symbols
	CPTMutableLineStyle *aaplLineStyle = [aaplPlot.dataLineStyle mutableCopy];
	aaplLineStyle.lineWidth = 1.0;
	aaplLineStyle.lineColor = aaplColor;
	aaplPlot.dataLineStyle = aaplLineStyle;
	CPTMutableLineStyle *aaplSymbolLineStyle = [CPTMutableLineStyle lineStyle];
	aaplSymbolLineStyle.lineColor = aaplColor;
	CPTPlotSymbol *aaplSymbol = [CPTPlotSymbol starPlotSymbol];
	aaplSymbol.fill = [CPTFill fillWithColor:aaplColor];
	aaplSymbol.lineStyle = aaplSymbolLineStyle;
	aaplSymbol.size = CGSizeMake(3.0f, 3.0f);
	aaplPlot.plotSymbol = aaplSymbol;   
//	CPTMutableLineStyle *googLineStyle = [googPlot.dataLineStyle mutableCopy];
//	googLineStyle.lineWidth = 0.5;
//	googLineStyle.lineColor = googColor;
//	googPlot.dataLineStyle = googLineStyle;
//	CPTMutableLineStyle *googSymbolLineStyle = [CPTMutableLineStyle lineStyle];
//	googSymbolLineStyle.lineColor = googColor;
//	CPTPlotSymbol *googSymbol = [CPTPlotSymbol starPlotSymbol];
//	googSymbol.fill = [CPTFill fillWithColor:googColor];
//	googSymbol.lineStyle = googSymbolLineStyle;
//	googSymbol.size = CGSizeMake(6.0f, 6.0f);
//	googPlot.plotSymbol = googSymbol;       
//	CPTMutableLineStyle *msftLineStyle = [msftPlot.dataLineStyle mutableCopy];
//	msftLineStyle.lineWidth = 1.0;
//	msftLineStyle.lineColor = msftColor;
//	msftPlot.dataLineStyle = msftLineStyle;  
//	CPTMutableLineStyle *msftSymbolLineStyle = [CPTMutableLineStyle lineStyle];
//	msftSymbolLineStyle.lineColor = msftColor;
//	CPTPlotSymbol *msftSymbol = [CPTPlotSymbol diamondPlotSymbol];
//	msftSymbol.fill = [CPTFill fillWithColor:msftColor];
//	msftSymbol.lineStyle = msftSymbolLineStyle;
//	msftSymbol.size = CGSizeMake(6.0f, 6.0f);
//	msftPlot.plotSymbol = msftSymbol;      
}

-(void)configureAxes {
	// 1 - Create styles
	CPTMutableTextStyle *axisTitleStyle = [CPTMutableTextStyle textStyle];
	axisTitleStyle.color = [CPTColor blackColor];
	axisTitleStyle.fontName = @"Helvetica";
	axisTitleStyle.fontSize = 10.0f;
	CPTMutableLineStyle *axisLineStyle = [CPTMutableLineStyle lineStyle];
	axisLineStyle.lineWidth = 0.5f;
	axisLineStyle.lineColor = [CPTColor blackColor];
	CPTMutableTextStyle *axisTextStyle = [[CPTMutableTextStyle alloc] init];
	axisTextStyle.color = [CPTColor blackColor];
	axisTextStyle.fontName = @"Helvetica";
	axisTextStyle.fontSize = 9.0f;
	CPTMutableLineStyle *tickLineStyle = [CPTMutableLineStyle lineStyle];
	tickLineStyle.lineColor = [CPTColor blackColor];
	tickLineStyle.lineWidth = 0.5f;
	CPTMutableLineStyle *gridLineStyle = [CPTMutableLineStyle lineStyle];
	tickLineStyle.lineColor = [CPTColor blackColor];
	tickLineStyle.lineWidth = 0.5f;
	// 2 - Get axis set
	CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostView.hostedGraph.axisSet;
	// 3 - Configure x-axis
	CPTAxis *x = axisSet.xAxis;
	x.title = @"Intervalo horario";
	x.titleTextStyle = axisTitleStyle;    
	x.titleOffset = 12.0f;
	x.axisLineStyle = axisLineStyle;
	x.labelingPolicy = CPTAxisLabelingPolicyNone;
	x.labelTextStyle = axisTextStyle;    
	x.majorTickLineStyle = axisLineStyle;
	x.majorTickLength = 4.0f;
	x.tickDirection = CPTSignNegative;
	CGFloat dateCount = [self.tiempoRuta count];
	NSMutableSet *xLabels = [NSMutableSet setWithCapacity:dateCount];
	NSMutableSet *xLocations = [NSMutableSet setWithCapacity:dateCount]; 
	NSInteger i = 0;
	for (int index = 0; index < [self.tiempoRuta count]; index++) {
        CGFloat location = i++;
        NSString *date = [self.tiempoRuta objectAtIndex:index];
        if (index == 0 ||
            index == [self.tiempoRuta count] / 2 ||
            index == [self.tiempoRuta count] - 1) {
            
            CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:date  textStyle:x.labelTextStyle];
            label.tickLocation = CPTDecimalFromCGFloat(location);
            label.offset = x.majorTickLength;
            if (label) {
                [xLabels addObject:label];
                [xLocations addObject:[NSNumber numberWithFloat:location]];
            }
        }
	}
	x.axisLabels = xLabels;
	x.majorTickLocations = xLocations;
	// 4 - Configure y-axis
	CPTAxis *y = axisSet.yAxis;    
	y.title = @"Altitud (m)";
	y.titleTextStyle = axisTitleStyle;
	y.titleOffset = -40.0f;       
	y.axisLineStyle = axisLineStyle;
	y.majorGridLineStyle = gridLineStyle;
	y.labelingPolicy = CPTAxisLabelingPolicyNone;
	y.labelTextStyle = axisTextStyle;    
	y.labelOffset = 16.0f;
	y.majorTickLineStyle = axisLineStyle;
	y.majorTickLength = 4.0f;
	y.minorTickLength = 2.0f;    
	y.tickDirection = CPTSignPositive;
	NSInteger majorIncrement = 50;
	NSInteger minorIncrement = 25;
    NSInteger yMin = [[NSString stringWithFormat:@"%.00f", altitudMin] integerValue];
	CGFloat yMax = altitudMax + 10.0f;//700.0f;  // should determine dynamically based on max price
    
	NSMutableSet *yLabels = [NSMutableSet set];
	NSMutableSet *yMajorLocations = [NSMutableSet set];
	NSMutableSet *yMinorLocations = [NSMutableSet set];
	for (NSInteger j = yMin; j <= yMax; j += minorIncrement) {
		NSUInteger mod = j % majorIncrement;
		if (mod == 0) {
			CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:[NSString stringWithFormat:@"%li", (long)j] textStyle:y.labelTextStyle];
			NSDecimal location = CPTDecimalFromInteger(j); 
			label.tickLocation = location;
			label.offset = -y.majorTickLength - y.labelOffset;
			if (label) {
				[yLabels addObject:label];
			}
			[yMajorLocations addObject:[NSDecimalNumber decimalNumberWithDecimal:location]];
		} else {
			[yMinorLocations addObject:[NSDecimalNumber decimalNumberWithDecimal:CPTDecimalFromInteger(j)]];
		}
	}
	y.axisLabels = yLabels;    
	y.majorTickLocations = yMajorLocations;
	y.minorTickLocations = yMinorLocations; 
}

#pragma mark - Rotation
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

#pragma mark - CPTPlotDataSource methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
	return [self.tiempoRuta count];
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
	NSInteger valueCount = [self.tiempoRuta count];
	switch (fieldEnum) {
		case CPTScatterPlotFieldX:
			if (index < valueCount) {
				return [NSNumber numberWithUnsignedInteger:index];
			}
			break;
			
		case CPTScatterPlotFieldY:
			if ([plot.identifier isEqual:CPDTickerSymbolAAPL] == YES) {
				return [self.altitudesRuta objectAtIndex:index];
//			} else if ([plot.identifier isEqual:CPDTickerSymbolGOOG] == YES) {
//				return [[[CPDStockPriceStore sharedInstance] monthlyPrices:CPDTickerSymbolGOOG] objectAtIndex:index];               
//			} else if ([plot.identifier isEqual:CPDTickerSymbolMSFT] == YES) {
//				return [[[CPDStockPriceStore sharedInstance] monthlyPrices:CPDTickerSymbolMSFT] objectAtIndex:index];               
			}
			break;
	}
	return [NSDecimalNumber zero];
}

@end
