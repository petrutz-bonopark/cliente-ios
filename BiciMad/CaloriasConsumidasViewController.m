//
//  CaloriasConsumidasViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 04/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "CaloriasConsumidasViewController.h"

@interface CaloriasConsumidasViewController ()

@end

@implementation CaloriasConsumidasViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.tipoDato = TIPO_DATO_CAL;
    [self getRutasUsuario];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
