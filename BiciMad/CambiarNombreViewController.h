//
//  CambiarNombreViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 31/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComunObject.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFHTTPClient.h>

@interface CambiarNombreViewController : UITableViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *textNombre;
@property (strong, nonatomic) IBOutlet UITextField *textApellido1;
@property (strong, nonatomic) IBOutlet UITextField *textApellido2;
@property (strong, nonatomic) IBOutlet UITextField *textEmail;
@property (strong, nonatomic) IBOutlet UITextField *textTelefono;
@property (strong, nonatomic) IBOutlet UITextField *textDireccion;
@property (strong, nonatomic) IBOutlet UITextField *textMunicipio;
@property (strong, nonatomic) IBOutlet UITextField *textProvincia;
@property (strong, nonatomic) IBOutlet UITextField *textCodPostal;

@property (strong, nonatomic) IBOutlet UITextField *passwordActual;
@property (strong, nonatomic) IBOutlet UITextField *passwordNuevo;
@property (strong, nonatomic) IBOutlet UITextField *passwordConfirmar;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton;

- (IBAction)cambiarPassword:(id)sender;
@end
