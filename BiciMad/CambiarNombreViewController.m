//
//  CambiarNombreViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 31/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "CambiarNombreViewController.h"

@interface CambiarNombreViewController ()

@end

@implementation CambiarNombreViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.textNombre.text = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_NOMBRE];
    
    self.textApellido1.text = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO];
    
    self.textApellido2.text = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO2];
    
    self.textEmail.text = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_EMAIL];
    
    self.textDireccion.text = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DIRECCION];
    
    self.textMunicipio.text = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_MUNICIPIO];
    
    self.textProvincia.text = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_PROVINCIA];
    
    self.textCodPostal.text = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_COD_POSTAL];
    
    self.textTelefono.text = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_TELEFONO];
    
    [self.passwordActual becomeFirstResponder];
}

- (void) viewDidDisappear:(BOOL)animated
{

    if (self.textNombre.text.length > 0) {
        [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_NOMBRE value:self.textNombre.text];
    }
    if (self.textApellido1.text.length > 0) {
        [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_APELLIDO value:self.textApellido1.text];
    }
    if (self.textApellido2.text.length > 0) {
        [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_APELLIDO2 value:self.textApellido2.text];
    }
    if (self.textEmail.text.length > 0) {
        [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_EMAIL value:self.textEmail.text];
    }
    if (self.textDireccion.text.length > 0) {
        [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_DIRECCION value:self.textDireccion.text];
    }
    if (self.textMunicipio.text.length > 0) {
        [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_MUNICIPIO value:self.textMunicipio.text];
    }
    if (self.textProvincia.text.length > 0) {
        [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_PROVINCIA value:self.textProvincia.text];
    }
    if (self.textCodPostal.text.length > 0) {
        [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_COD_POSTAL value:self.textCodPostal.text];
    }
    if (self.textTelefono.text.length > 0) {
        [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_TELEFONO value:self.textTelefono.text];
    }
    
    if (self.passwordActual.text.length > 0)
    {
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"cambioNombre" object:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}

- (BOOL) comprobarPassword
{

    if (self.passwordNuevo.text.length > 5
        && [self.passwordNuevo.text isEqualToString:self.passwordConfirmar.text])
        {
            return TRUE;
        } else {
            return FALSE;
        }
}

- (IBAction)cambiarPassword:(id)sender {

    [self.passwordActual resignFirstResponder];
    [self.passwordNuevo resignFirstResponder];
    [self.passwordConfirmar resignFirstResponder];
    
    if ([self comprobarPassword]) {
            
        MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            progressHUD.dimBackground = YES;
            progressHUD.labelText = @"Cambiando contraseña...";
            
            NSDictionary *myJson=@{TAG_DNI : [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DNI],
                                   TAG_PASSWORD : self.passwordActual.text,
                                   TAG_NEW_PASSWORD : self.passwordNuevo.text};
            
            NSURL *url = [NSURL URLWithString:BaseURLString];
            AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
            httpClient.parameterEncoding = AFJSONParameterEncoding;
            NSDictionary *params = myJson ;
            
            [httpClient postPath:url_change_password parameters:params
                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                             // Print the response body in text
                             NSLog(@"Write to DB: Success");
                             NSError *error ;
                             NSDictionary* jsonFromData = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                             NSLog(@"Return string: %@", jsonFromData);
                             
                             // TODO: Realizar el cambio de contraseña y si es correcto enviar email.
                             // NSString *success = [jsonFromData objectForKey:TAG_SUCCESS];
                             NSString *message = [jsonFromData objectForKey:TAG_MESSAGE];
                             
                             if (jsonFromData != NULL) {
                                 [ComunObject alertErrorMessage:@"TITULO_ALERT_ERROR" texto:message];

                             } else {
                                 UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Cambio de contraseña en BiciMAD" message:@"Su contraseña ha sido actualizada en los servidores de BiciMAD." delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
                                 alert.alertViewStyle = UIAlertViewStyleDefault;
                                 alert.tag = 1;
                                 
                                 [alert show];
                                 
                             }
                             [progressHUD hide:YES];
                             
                             
                         }
                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                             NSLog(@"Write to DB: Fail");
                             [progressHUD hide:YES];
                             
                             [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"LABEL_ERROR_CONNECTION"];
                             
                         }];
        } else {
            [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"MENSAJE_ERROR_PASSWORD"];
        }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{

    if (textField == self.textNombre) {
        [textField resignFirstResponder];
        [self.textApellido1 becomeFirstResponder];
    } else if (textField == self.textApellido1) {
        [textField resignFirstResponder];
        [self.textApellido2 becomeFirstResponder];
    } else if (textField == self.textApellido2) {
        [textField resignFirstResponder];
    } else if (textField == self.passwordActual) {
        [textField resignFirstResponder];
        [self.passwordNuevo becomeFirstResponder];
    } else if (textField == self.passwordNuevo) {
        [textField resignFirstResponder];
        [self.passwordConfirmar becomeFirstResponder];
    } else if (textField == self.passwordConfirmar) {
        [textField resignFirstResponder];
    } else if (textField == self.textDireccion) {
        [textField resignFirstResponder];
        [self.textMunicipio becomeFirstResponder];
    } else if (textField == self.textMunicipio) {
        [textField resignFirstResponder];
        [self.textProvincia becomeFirstResponder];
    } else if (textField == self.textProvincia) {
        [textField resignFirstResponder];
        [self.textCodPostal becomeFirstResponder];
    } else if (textField == self.textCodPostal) {
        [textField resignFirstResponder];
    } else if (textField == self.textTelefono) {
        [textField resignFirstResponder];
    }
    
    return NO;
}

@end
