//
//  ClubSocioVC.m
//  BiciMad
//
//  Created by Miguel Asuar on 04/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "ClubSocioVC.h"

@interface ClubSocioVC ()

@end

@implementation ClubSocioVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
