//
//  CompraAbonoViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 31/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebFormTPVViewController.h"
#import "PreferenciasUsuarios.h"

@interface CompraAbonoViewController : UIViewController<UITextFieldDelegate>

@property (assign, nonatomic) NSInteger importePago;
@property (assign, nonatomic) NSString * nombreUsuario;
@property (assign, nonatomic) NSString * apellidoUsuario;
@property (assign, nonatomic) NSString * apellido2Usuario;
@property (assign, nonatomic) NSString * emailUsuario;
@property (strong, nonatomic) NSString * numTarjetaTransporte;

@property (strong, nonatomic) IBOutlet UISwitch *switchTarjetaMadrid;
@property (strong, nonatomic) IBOutlet UIImageView *imagenTarjeta;
@property (strong, nonatomic) IBOutlet UITextField *datoTarjeta1;
@property (strong, nonatomic) IBOutlet UILabel *importe15;
@property (strong, nonatomic) IBOutlet UILabel *labelImporte15;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewDatosTarjeta;

@property (strong, nonatomic) IBOutlet UILabel *labelNormal;
@property (strong, nonatomic) IBOutlet UILabel *importe25;
@property (strong, nonatomic) IBOutlet UILabel *labelImporte25;

- (IBAction)cambiarTarjetaMadrid:(id)sender;
- (IBAction)dismissPago:(id)sender;

@end
