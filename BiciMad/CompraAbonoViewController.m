//
//  CompraAbonoViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 31/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "CompraAbonoViewController.h"

@interface CompraAbonoViewController ()

@end

@implementation CompraAbonoViewController

@synthesize nombreUsuario;
@synthesize apellido2Usuario;
@synthesize apellidoUsuario;
@synthesize emailUsuario;
@synthesize importePago;
@synthesize numTarjetaTransporte;

CGPoint initialFramePoint;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.nombreUsuario = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_NOMBRE];
    self.apellidoUsuario = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO];
    self.apellido2Usuario = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO2];
    self.emailUsuario = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_EMAIL];
    self.numTarjetaTransporte = @"NO";
    
    initialFramePoint = CGPointMake(initialScrollAltaX, initialScrollAltaY);
    
    [self.scrollViewDatosTarjeta setContentOffset:initialFramePoint animated:NO];
}

- (void) viewDidAppear:(BOOL)animated
{
    [self.scrollViewDatosTarjeta setContentOffset:initialFramePoint animated:NO];
    [super viewDidAppear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cambiarTarjetaMadrid:(id)sender {
    
    if ([self.switchTarjetaMadrid isOn]) {
        self.scrollViewDatosTarjeta.hidden = NO;
        
        self.labelNormal.hidden = YES;
        self.importe25.hidden = YES;
        self.labelImporte25.hidden = YES;
        
    } else {
        self.scrollViewDatosTarjeta.hidden = YES;
        
        self.labelNormal.hidden = NO;
        self.importe25.hidden = NO;
        self.labelImporte25.hidden = NO;
        
    }
}

- (IBAction)dismissPago:(id)sender {
    
    [self dismissViewControllerAnimated:TRUE completion:Nil];
}

- (IBAction)goToResumenCompra:(id)sender {
    
    if ([self.switchTarjetaMadrid isOn]) {
        NSString *numTarjeta = [self.datoTarjeta1.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        if (numTarjeta.length == 13 || numTarjeta.length == 22) {
            self.importePago = 15;
            
            self.numTarjetaTransporte = numTarjeta;
            [self consultarTarjetaTransporte];
            
        } else {
            [ComunObject alertError:@"MENSAJE_ERROR_LONGITUD_CAMPOS_TITULO" idTexto:@"MENSAJE_ERROR_LONGITUD_CAMPO_1"];
        }
        
    } else {
        self.importePago = 25;
        self.numTarjetaTransporte = @"NO";
        [self performSegueWithIdentifier:@"webFormTPV" sender:self];
        
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"webFormTPV"]) {
      
        WebFormTPVViewController * webFormTPV = segue.destinationViewController;
        webFormTPV.importePago = self.importePago;
        webFormTPV.nombreUsuario = self.nombreUsuario;
        webFormTPV.apellidoUsuario = self.apellidoUsuario;
        webFormTPV.apellido2Usuario = self.apellido2Usuario;
        webFormTPV.emailUsuario = self.emailUsuario;
        webFormTPV.tipoOperacion = TIPO_PAGO_ALTA;
        webFormTPV.numeroTarjetaTransporte = self.numTarjetaTransporte;
        
    }
}

- (void) consultarTarjetaTransporte
{
    // Si el usuario ha marcado que posee la tarjeta del consorcio de transporte se debe realizar una consulta a sus servidores para verificar que el usuario existe.
    
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    progressHUD.dimBackground = YES;
    progressHUD.labelText = @"Conectando con el servidor...";
    NSString *urlInfoTarjetaTransporte = [NSString stringWithFormat:@"%@%@?num_tarjeta_transporte=%@", BaseURLString, url_get_info_tarjeta_consorcio, numTarjetaTransporte];
    NSURL  *url = [NSURL  URLWithString:urlInfoTarjetaTransporte];
    NSURLRequest  *request = [NSURLRequest  requestWithURL:url];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest  *request, NSHTTPURLResponse  *response, id JSON) {
                                                        NSLog(@"Write to DB: Success");
                                                        NSLog(@"Return string: %@", JSON);
                                                        
                                                        NSString *success = [JSON objectForKey:TAG_SUCCESS];
                                                        NSString *message = [JSON objectForKey:TAG_MESSAGE];
                                                        
                                                        if ([success integerValue] == 1) {
                                                            [self performSegueWithIdentifier:@"webFormTPV" sender:self];
                                                        } else {
                                                            [ComunObject alertErrorMessage:@"TITULO_ALERT_ERROR" texto:message];
                                                        }
                                                        // Ocultamos el progressHUD
                                                        [progressHUD hide:YES];
                                                        
                                                    }
                                                    failure:^(NSURLRequest  *request, NSHTTPURLResponse  *response, NSError  *error, id JSON) {
                                                        [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"LABEL_ERROR_CONNECTION"];
                                                        
                                                        [progressHUD hide:YES];
                                                    }];
    
    [operation start];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    if (textField == self.datoTarjeta1) {
        [textField resignFirstResponder];
        [self.scrollViewDatosTarjeta setContentOffset:initialFramePoint animated:YES];
    }
    
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.scrollViewDatosTarjeta setContentOffset:CGPointMake(0, [self.scrollViewDatosTarjeta convertPoint:CGPointZero fromView:textField].y - valorSubidaTextField) animated:YES];
}

@end
