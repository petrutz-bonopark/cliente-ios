//
//  ComunGPSViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 06/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSLocationManager.h"
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD.h>
#import "CustomIOS7AlertView.h"
#import "ComunObject.h"
#import "ComunMapasViewController.h"

@interface ComunGPSViewController : ComunMapasViewController <PSLocationManagerDelegate, UIActionSheetDelegate>

{
    PSLocationManager *locationManager;
    IBOutlet UILabel *time;
    NSTimer *timer;
    int MainInt;
    
    int seconds;
    int minutes;
    int hours;
    
    float calorias;
    float peso;
    float velocidadMedia;
    float velocidadMax;
    float duracionTotal;
    float coeficienteCalorico;
    float distanciaTotal;
    float huellaCO2;
    
    NSMutableArray* locationHistory;
    NSDictionary * indiceCalorico;
    
}
@property (weak, nonatomic) IBOutlet UILabel *myStrength;
@property (weak, nonatomic) IBOutlet UILabel *myDistance;
@property (weak, nonatomic) IBOutlet UILabel *mySpeed;
@property (weak, nonatomic) IBOutlet UILabel *statusGPS;
@property (weak, nonatomic) IBOutlet UILabel *labelDistance;
@property (weak, nonatomic) IBOutlet UILabel *labelSpeed;

@property (weak, nonatomic) IBOutlet UIButton *startGPS;
@property (weak, nonatomic) IBOutlet UIButton *stopGPS;

- (void)stopListenerGPS;
- (IBAction)startListenerGPS:(id)sender;

- (void) showActivity;

-(IBAction)showActionSheet:(id)sender;

@end
