//
//  ComunGPSViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 06/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "ComunGPSViewController.h"

@interface ComunGPSViewController ()

@end

@implementation ComunGPSViewController

NSString * escalaUnidadesUsuario;
bool isGettingGPS = false;
bool isShowingAlert = false;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Se define un evento para pausar el contador cuando la app pasa a segundo plano.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pause)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    
    escalaUnidadesUsuario = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_ESCALA_UNIDADES];
    if ([escalaUnidadesUsuario isEqualToString:@"km"]) {
        _labelDistance.text = @"km";
        _labelSpeed.text = @"km/h";
    } else {
        _labelDistance.text = @"mi";
        _labelSpeed.text = @"mi/h";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark PSLocationManagerDelegate

- (void)locationManager:(PSLocationManager *)locationManager signalStrengthChanged:(PSLocationManagerGPSSignalStrength)signalStrength {
    NSString *strengthText;
    if (signalStrength == PSLocationManagerGPSSignalStrengthWeak) {
        strengthText = NSLocalizedString(@"Weak", @"");
        [self.statusGPS setTextColor:[UIColor orangeColor]];
    } else if (signalStrength == PSLocationManagerGPSSignalStrengthStrong) {
        strengthText = NSLocalizedString(@"Strong", @"");
        [self.statusGPS setTextColor:[UIColor greenColor]];
    } else {
        strengthText = NSLocalizedString(@"...", @"");
        [self.statusGPS setTextColor:[UIColor redColor]];
    }
    
    self.myStrength.text = strengthText;
    
}

- (void)locationManagerSignalConsistentlyWeak:(PSLocationManager *)locationManager {
    self.myStrength.text = NSLocalizedString(@"Consistently Weak", @"");
}

- (void)locationManager:(PSLocationManager *)locationManager distanceUpdated:(CLLocationDistance)distance {
    // Distancia en metros
    float currentDistance = distance / 1000;
    // Si el usuario elige millas se hace la conversión
    if ([escalaUnidadesUsuario isEqualToString:@"mi"]) {
        currentDistance = currentDistance * millasXKM;
    }
    self.myDistance.text = [NSString stringWithFormat:@"%.2f", currentDistance];
}

- (void)locationManager:(PSLocationManager *)locationManager error:(NSError *)error {
    // location services is probably not enabled for the app
    self.myStrength.text = NSLocalizedString(@"Unable to determine location", @"");
}

-(void) locationManager:(PSLocationManager *)locationManager waypoint:(CLLocation *)waypoint calculatedSpeed:(double)calculatedSpeed
{
    if (calculatedSpeed >= 0)
    {
        // Velocidad en m/s
        float currentSpeed = calculatedSpeed * mXsAkmXh;
        
        if ([escalaUnidadesUsuario isEqualToString:@"mi"]) {
            currentSpeed = currentSpeed * millasXKM;
        }
        
        self.mySpeed.text = [NSString stringWithFormat:@"%.2f", currentSpeed];
    }
}

// Método que para la recepción de datos desde el GPS
- (void) stopListenerGPS {
    [self registrarRecorridoEnBD];
    
    [[PSLocationManager sharedLocationManager] stopLocationUpdates];
    isGettingGPS = false;
    
    [self.startGPS setHidden:NO];
    [self.stopGPS setHidden:YES];
    
    [self stop];
    [self reset];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

// Método que inicia la recepción de datos desde el GPS
- (IBAction)startListenerGPS:(id)sender {
    locationHistory = [[NSMutableArray alloc] init];
    
    [[PSLocationManager sharedLocationManager] startLocationUpdates];
    isGettingGPS = true;
    [self.startGPS setHidden:YES];
    [self.stopGPS setHidden:NO];
    
    [self start];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

// Método que inicia el contador de la sesión
-(void)start {
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                             target:self
                                           selector:@selector(showActivity)
                                           userInfo:nil
                                            repeats:YES];
}

// Método que para el contador de la sesión.
- (void) stop
{
    [timer invalidate];
}

- (void) pause
{
    // Se pausa cuando se estan capturando datos del GPS y no está el diálogo en pantalla
    if (isGettingGPS && !isShowingAlert){
        [timer invalidate];
        [[PSLocationManager sharedLocationManager] stopLocationUpdates];
        [self.startGPS setHidden:NO];
        [self.stopGPS setHidden:YES];
        [self showActionSheet:[[UIButton alloc] init]];
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}

- (void)reset
{
    time.text = @"00:00:00";
    hours = 0;
    seconds = 0;
    minutes = 0;
}

// Método que calcula el formato del contador.
- (void) showActivity
{
    seconds++;
    if (seconds == 60)
    {
        seconds = 0;
        minutes++;
        if (minutes == 60)
        {
            minutes = 0;
            hours++;
        }
    }
    time.text = [NSString stringWithFormat:@"%02i:%02i:%02i",hours,minutes,seconds];
}

// Método que guarda en BD los datos de la ruta y las posiciones.
- (void) registrarRecorridoEnBD
{
    // Mostrar progressHUD mientras se registra la ruta en BD
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    progressHUD.dimBackground = YES;
    progressHUD.labelText = @"Guardando la ruta...";
    
    // Todos los toques de pantalla deshabilitan el progressHUD
    progressHUD.userInteractionEnabled = YES;
    
    // Guardamos en formato JSON las coordenadas de la ruta
    NSMutableArray * arrayDeCoordenadas = [[NSMutableArray alloc] init];
    NSDictionary *coordenadasJSON = [[NSDictionary alloc] init];
    locationHistory = [[PSLocationManager sharedLocationManager] getLocationHistory];
    
    for (CLLocation *location in locationHistory) {
        coordenadasJSON = [NSDictionary dictionaryWithObjectsAndKeys:
                           [NSString stringWithFormat:@"%f", location.coordinate.latitude], @"latitude",
                           [NSString stringWithFormat:@"%f", location.coordinate.longitude], @"longitude",
                           [NSString stringWithFormat:@"%f", location.altitude], @"altitude",
                           [ComunObject formatDateFromDate:location.timestamp pattern:@"yyyy-MM-dd HH:mm:ss"], @"timestamp",
                           [NSString stringWithFormat:@"%f", location.speed], @"speed", nil];
        
        [arrayDeCoordenadas addObject:coordenadasJSON];
    }
    
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:arrayDeCoordenadas options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    
    // Se calcula las velocidades media y máxima
    [self calcularVelocidadMediaYMax];
    
    // Se calcula la duración total en segundos
    [self calcularDuracionTotal];
    
    // Se calcula la distancia total recorrida
    [self calcularDistanciaTotal];
    
    // Se calcula las calorias para esta ruta
    [self calcularCalorias];
    
    // Se calcula huella de CO2 evitada
    [self calcularHuellaCO2];
    
    // Primer evento de la ruta para obtener la fecha
    CLLocation *primeraLocalizacion = [locationHistory objectAtIndex:0];
    NSString * fechaIni = [ComunObject formatDateFromDate:primeraLocalizacion.timestamp pattern:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDictionary *myJson=@{TAG_DNI: [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DNI],
                           TAG_ID_AUTH: [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_ID_AUTH],
                           TAG_FECHA: fechaIni,
                           TAG_KM: [NSString stringWithFormat:@"%.2f", distanciaTotal],
                           TAG_VELOCIDAD_MEDIA: [NSString stringWithFormat:@"%.2f", velocidadMedia],
                           TAG_VELOCIDAD_MAX: [NSString stringWithFormat:@"%.2f", velocidadMax],
                           TAG_DURACION: [NSString stringWithFormat:@"%.2f", duracionTotal],
                           TAG_CALORIAS: [NSString stringWithFormat:@"%.2f", calorias],
                           TAG_HUELLA_CO2: [NSString stringWithFormat:@"%.2f", huellaCO2],
                           TAG_COORDENADAS_RUTA: jsonString};
    
    
    NSString *saveRuta = [NSString  stringWithFormat:@"%@%@?format=json", BaseURLString, url_save_ruta];
    NSURL *url = [NSURL  URLWithString:saveRuta];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFJSONParameterEncoding;
    NSDictionary *params = myJson ;
    
    [httpClient postPath:url_save_ruta parameters:params
                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                     // Print the response body in text
                     NSLog(@"Write to DB: Success");
                     NSError *error ;
                     NSDictionary* jsonFromData = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                     NSLog(@"Return string: %@", jsonFromData);
                     
                     // Ocultamos el progressHUD
                     [progressHUD hide:YES];
                     
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Write to DB: Fail");
                     [progressHUD hide:YES];
                     
                     [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"LABEL_ERROR_CONNECTION"];
                 }];
}

- (void) calcularVelocidadMediaYMax
{
    velocidadMedia = 0;
    velocidadMax = 0;
    
    if (locationHistory.count > 0) {
        for (CLLocation *location in locationHistory) {
            if (velocidadMax < location.speed) {
                velocidadMax = location.speed;
            }
        }
    
        float velocidadMetrosXSegundo = (distanciaTotal * 1000) / duracionTotal; // Se obtienen los metros x segundo
        if (velocidadMetrosXSegundo > 0) {
            velocidadMedia = velocidadMetrosXSegundo * mXsAkmXh; // Se obtienen los km por hora
        }
    }
}

- (void) calcularDuracionTotal
{
    // Tiempo total del recorrido
    NSArray* arrayDuracion = [time.text componentsSeparatedByString: @":"];
    
    //TODO: Tiempo total en segundos
    duracionTotal = [arrayDuracion[2] floatValue] + [arrayDuracion[1] floatValue] * 60 + [arrayDuracion[0] floatValue] * 3600;
}

- (void) calcularDistanciaTotal
{
    // Obtenemos la distancia total recorrida
    // Guardamos el valor con solamente 2 decimales
    distanciaTotal = [self.myDistance.text floatValue];
}

- (void) calcularCalorias
{
    calorias = 0.0;
    
    // Peso medio de una persona 70KG
    peso = 70.0;
    
    coeficienteCalorico = [self getIndiceCaloricoFromVelocidad];
    
    calorias = peso * coeficienteCalorico * (duracionTotal/60);
    
}

- (void) calcularHuellaCO2
{
    // La huella de CO2 se calcula multiplicando los km recorridos por el consumo medio aproximado de un coche cada kilómetro.
    huellaCO2 = (distanciaTotal) * kgCO2PorKm;
}

- (float) getIndiceCaloricoFromVelocidad
{
    
    float indice = 0.0;
    bool encontrado = false;
    
    NSArray * keys=[indiceCalorico allKeys];
    keys = [keys sortedArrayUsingSelector: @selector(compare:)];
    
    for (NSString *key in keys) {
        if (!encontrado && velocidadMedia <= [key floatValue]) {
            indice = [[indiceCalorico objectForKey:key] floatValue];
            encontrado = true;
        }
    }
    
    return indice;
}

-(IBAction)showActionSheet:(id)sender {
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Terminar ruta" delegate:self
                                                   cancelButtonTitle:@"Continuar"
                                              destructiveButtonTitle:@"Detener"
                                                   otherButtonTitles:nil];
    popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [popupQuery showInView:self.view];
    
    isShowingAlert = true;
    [self stop];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    /**
     * OR use the following switch statement
     * Suggested by Colin =)
     */
    
     switch (buttonIndex) {
     case 0:
        [self stopListenerGPS];
     break;
     case 1:
        [self startListenerGPS:self];
     break;

     }
    
    isShowingAlert = false;
}
@end
