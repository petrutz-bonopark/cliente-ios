//
//  ComunMapasViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 06/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD.h>

#import "BMStepsDelegate.h"
#import "CustomIOS7AlertView.h"

#import "XYPieChart.h"
#import "PieChartOcupacionViewController.h"

#import "InfoWindowsView.h"

@interface ComunMapasViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, BMStepsDelegate, GMSMapViewDelegate>

@property (nonatomic, weak) NSDictionary *estaciones;
@property (nonatomic, strong) NSMutableArray *estacionesList;
@property (nonatomic, strong) NSMutableArray *markerList;
@property (nonatomic, strong) NSMutableArray *estacionesCercanas;
@property (nonatomic, strong) NSString *tipoDatos;

@property (retain, nonatomic) IBOutlet UIBarButtonItem *barItemSearch;

@property (strong, nonatomic) PieChartOcupacionViewController *pieChartOcupacion;
@property (strong, nonatomic) IBOutlet UIView *viewLeyenda;
@property (strong, nonatomic) IBOutlet UIView *viewAyudaUsuario;
- (IBAction)ocultarAyuda:(id)sender;

- (void)setMyLocation:(CLLocation *)myLocation placeReference:(NSString *)placeReference;
- (IBAction)searchPlaces:(id)sender;

- (IBAction)backgroundTap;
- (IBAction)actualizarEstaciones:(id)sender;
@end
