//
//  ComunObject.h
//  BiciMad
//
//  Created by Miguel Asuar on 07/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <tgmath.h>
#import <GoogleMaps/GoogleMaps.h>
#import "TCGoogleDirections.h"
#import "CustomIOS7AlertView.h"
#import "PreferenciasUsuarios.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define ColorApp 0x2c71c1
#define ColorApp2 0xf35b38

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

static int sizeMenuIPHONE = 250;
static int sizeMenuIPAD = 400;

static int yNumEstacionIPHONE = 6;
static int yNumEstacionIPAD = 10;

static int xNumEstacionIPHONE1 = 10;
static int xNumEstacionIPHONE2 = 7;
static int xNumEstacionIPHONE3 = 4;

static int xNumEstacionIPAD1 = 15;
static int xNumEstacionIPAD2 = 12;
static int xNumEstacionIPAD3 = 9;

static float xInfoWindowIPHONE = 0.89f;
static float yInfoWindowIPHONE = 0.52f;

static float xInfoWindowIPAD = 1.1f;
static float yInfoWindowIPAD = 0.75f;

// Alert ocupación estación
static int widthAlertEstacionIPHONE = 290;
static int heightAlertEstacionIPHONE = 400;

static int widthAlertEstacionIPAD = 500;
static int heightAlertEstacionIPAD = 700;

static float xBotonesIPHONE = 25.0f;
static float yBotonesIPHONE = 300.0f;
static float widthBotonesIPHONE = 240.0f;
static float heightBotonesIPHONE = 37.0f;

static float xBotonesIPAD = 50.0f;
static float yBotonesIPAD = 600.0f;
static float widthBotonesIPAD = 400.0f;
static float heightBotonesIPAD = 37.0f;

static float fontSizeTituloIPHONE = 11.0f;
static float fontSizeTituloIPAD = 20.0f;

static NSString  *const BaseURLString = @"http://5.56.56.139:16080/app/app/";

// URL to get contacts JSON
static NSString  *const url_all_estaciones = @"functions/get_all_estaciones.php";
static NSString  *const url_estaciones_cercanas = @"functions/get_estaciones_cercanas.php";
static NSString  *const url_save_ruta = @"functions/set_ruta_usuario.php";
static NSString  *const url_get_ruta_usuario = @"functions/get_historial_rutas_usuario.php";
static NSString  *const url_reservar_base = @"functions/set_base_reservada.php";
static NSString  *const url_save_new_user = @"functions/set_new_usuario.php";
static NSString  *const url_get_user = @"functions/get_usuario.php";
static NSString  *const url_get_user_by_dni = @"functions/get_usuario_por_dni.php";
static NSString  *const url_save_abono = @"functions/set_new_abono.php";
static NSString  *const url_get_ranking_usuarios = @"functions/get_ranking_usuarios.php";
static NSString  *const url_get_datos_usuario = @"functions/get_datos_usuario.php";
static NSString  *const url_update_user = @"functions/update_usuario.php";
static NSString  *const url_generate_password = @"functions/generate_new_password.php";
static NSString  *const url_change_password = @"functions/change_password.php";
static NSString  *const url_get_info_tarjeta_consorcio = @"functions/get_info_tarjeta_consorcio.php";
static NSString  *const url_submint_form_TPV = @"functions/submit_form_tpv.php";
static NSString  *const url_submint_form_TPV_tutor = @"functions/submit_form_tpv_tutor.php";
static NSString  *const url_submint_form_recarga_TPV = @"functions/submit_form_recarga_tpv.php";
static NSString  *const url_registrar_incidencia = @"functions/registrar_incidencia.php";

// JSON Node names
static NSString  *const TAG_SUCCESS = @"success";
static NSString  *const TAG_MESSAGE = @"message";

static NSString  *const TAG_ESTACIONES = @"estaciones";
static NSString  *const TAG_ID = @"idestacion";
static NSString  *const TAG_NAME = @"nombre";
static NSString  *const TAG_ADDRESS = @"direccion";
static NSString  *const TAG_LATITUDE = @"latitud";
static NSString  *const TAG_LONGITUDE = @"longitud";
static NSString  *const TAG_STATUS = @"activo";
static NSString  *const TAG_LUZ = @"luz";
static NSString  *const TAG_NUM_BASES = @"numero_bases";
static NSString  *const TAG_LIBRES = @"libres";
static NSString  *const TAG_PORCENTAJE = @"porcentaje";

static NSString  *const TAG_RUTAS = @"rutas";
static NSString  *const TAG_ID_RUTA = @"idruta";
static NSString  *const TAG_USUARIO = @"usuario";
static NSString  *const TAG_FECHA = @"fecha";
static NSString  *const TAG_KM = @"km";
static NSString  *const TAG_VELOCIDAD_MEDIA = @"velocidad_media";
static NSString  *const TAG_VELOCIDAD_MAX = @"velocidad_max";
static NSString  *const TAG_DURACION = @"duracion";
static NSString  *const TAG_CALORIAS = @"calorias_consumidas";
static NSString  *const TAG_HUELLA_CO2 = @"huella_CO2";
static NSString  *const TAG_COORDENADAS_RUTA = @"coordenadas_ruta";

static NSString  *const TAG_COORDENADAS_ALTITUDE = @"altitude";
static NSString  *const TAG_COORDENADAS_LATITUDE = @"latitude";
static NSString  *const TAG_COORDENADAS_LONGITUDE = @"longitude";
static NSString  *const TAG_COORDENADAS_TIMESTAMP = @"timestamp";
static NSString  *const TAG_COORDENADAS_SPEED = @"speed";

static NSString  *const TAG_TABLA_USUARIO = @"usuario";
static NSString  *const TAG_DNI = @"dni";
static NSString  *const TAG_ID_AUTH = @"id_auth";
static NSString  *const TAG_NOMBRE = @"nombre";
static NSString  *const TAG_APELLIDO1 = @"apellido1";
static NSString  *const TAG_APELLIDO2 = @"apellido2";
static NSString  *const TAG_EMAIL = @"email";
static NSString  *const TAG_TELEFONO = @"telefono";
static NSString  *const TAG_FECHA_NACIMIENTO = @"fecha_nacimiento";
static NSString  *const TAG_DIRECCION = @"direccion";
static NSString  *const TAG_MUNICIPIO = @"municipio";
static NSString  *const TAG_COD_POSTAL = @"codigo_postal";
static NSString  *const TAG_PROVINCIA = @"provincia";
static NSString  *const TAG_PASSWORD = @"password";
static NSString  *const TAG_NEW_PASSWORD = @"new_password";
static NSString  *const TAG_CODIGO_PUSH = @"codigo_push";

static NSString  *const TAG_IDRFID = @"idrfid";
static NSString  *const TAG_SALDO = @"saldo";
static NSString  *const TAG_TIPO = @"tipo";

static NSString  *const TAG_RANKINGS_USUARIO = @"rankings";
static NSString  *const TAG_USOS = @"usos";
static NSString  *const TAG_DISTANCIA_TOTAL = @"distancia_total";
static NSString  *const TAG_DURACION_TOTAL = @"duracion_total";
static NSString  *const TAG_CALORIAS_TOTAL = @"calorias_totales";
static NSString  *const TAG_HUELLA_CO2_TOTAL = @"huella_CO2_total";

static NSString  *const TAG_ESTADO_RESERVA = @"estado_reserva";
static NSString  *const TAG_NUM_TARJETA_TRANSPORTE = @"num_tarjeta_transporte";

static NSString  *const TAG_NOMBRE_INCIDENCIA = @"nombre_incidencia";
static NSString  *const TAG_DETALLE_INCIDENCIA = @"detalle_incidencia";
static NSString  *const TAG_ESTACION_INCIDENCIA = @"estacion_incidencia";
static NSString  *const TAG_DIRECCION_INCIDENCIA = @"direccion_incidencia";

// Control del tipo de dato mostrado en las tablas de rutas
static NSString *const TIPO_DATO_KM = @"KM";
static NSString *const TIPO_DATO_CAL = @"CAL";
static NSString *const TIPO_DATO_HUE = @"HUE";

static float const kgCO2PorKm = 0.748;
static float const millasXKM = 0.622;
static float const mXsAkmXh = 3.6;

static NSInteger const TIPO_PAGO_ALTA = 1;
static NSInteger const TIPO_PAGO_RECARGA = 2;

static NSInteger const TIPO_ALTA_BBDD_MOVIL = 5;

static int valorSubidaTextField = 25;
static int initialScrollAltaX = 0;
static int initialScrollAltaY = 0;

@interface ComunObject : NSObject

+ (void) alertError: (NSString *) idTitulo idTexto: (NSString *) idTexto;

+ (void) alertErrorMessage: (NSString *) idTitulo texto: (NSString *) texto;

+ (NSString *) formatDateFromString: (NSString *) dateString pattern: (NSString *) pattern;
+ (NSString *) formatDateFromDate: (NSDate *) date pattern: (NSString *) pattern;
+ (NSString *) formatDateFromString: (NSString *) dateString patternIN: (NSString *) patternIN patternOUT: (NSString *) patternOUT;

+ (NSString *) getCurrentDate;

+ (NSString *) formatTime: (NSString *) time;

+ (NSString *) formatTimeFromFloat: (float) time;

+ (void)drawRoute:(TCDirectionsRoute *)route onMap:(GMSMapView *)mapView;

+ (void) drawRouteWaypoints:(GMSMapView *) mapView coordenadas:(NSDictionary *) coordenadas;

+ (BOOL) isStringEmpty: (NSString *) string;

+ (void) savePreferenciasUsuario: (NSString *) dni nombre: (NSString*) nombre apellido: (NSString *) apellido apellido2:(NSString *) apellido2 fechaNac:(NSString *) fechaNac telefono: (NSString *) telefono email: (NSString *) email direccion: (NSString *) direccion municipio: (NSString *) municipio provincia: (NSString *) provincia codPostal: (NSString *) codPostal tipo: (NSString *) tipo unidadesMedida: (NSString *) unidadesMedida temperatura: (NSString *) temperatura saveSesion: (NSString *) saveSesion idAuth:(NSString *) idAuth;

+ (void) savePreferenciasTutorUsuario: (NSString *) dni nombre: (NSString*) nombre apellido: (NSString *) apellido apellido2:(NSString *) apellido2;

+ (void) removePreferenciasUsuario;

+ (NSString *) letraDNI: (NSString *) dni;

+ (BOOL) validateNif: (NSString *) nif;

+ (BOOL) isNifValido: (NSString *) nifIN;

+ (BOOL) isCifValido: (NSString *) cif;

+ (BOOL) isTarjetaDeResidenciaValida: (NSString *) cadena;

+ (BOOL) isNIEValido:(NSString *) nie;

+ (NSString *) generarCodigoRecogida:(int)len;

+ (BOOL) isUsuarioMayorDe: (NSInteger) edadPermitida birthday: (NSDate*) birthdate;

+ (BOOL) isUsuarioMenorDe: (NSInteger) edadPermitida birthday: (NSDate*) birthdate;
@end
