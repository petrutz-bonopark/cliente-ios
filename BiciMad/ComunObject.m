//
//  ComunObject.m
//  BiciMad
//
//  Created by Miguel Asuar on 07/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "ComunObject.h"

@implementation ComunObject

+ (void) alertError: (NSString *) idTitulo idTexto: (NSString *) idTexto
{
    UIAlertView * alert = [[UIAlertView alloc]
        initWithTitle:NSLocalizedStringFromTable(idTitulo, @"messages", nil)
        message:NSLocalizedStringFromTable(idTexto, @"messages", nil)
        delegate:self
        cancelButtonTitle:NSLocalizedStringFromTable(@"LABEL_CLOSE", @"messages", nil)
        otherButtonTitles:nil];
    alert.alertViewStyle = UIAlertViewStyleDefault;
    alert.tag = 1;
    
    [alert show];
    
}

+ (void) alertErrorMessage:(NSString *)idTitulo texto:(NSString *)texto
{
    UIAlertView * alert = [[UIAlertView alloc]
                           initWithTitle:NSLocalizedStringFromTable(idTitulo, @"messages", nil)
                           message:texto
                           delegate:self
                           cancelButtonTitle:NSLocalizedStringFromTable(@"LABEL_CLOSE", @"messages", nil)
                           otherButtonTitles:nil];
    alert.alertViewStyle = UIAlertViewStyleDefault;
    alert.tag = 1;
    
    [alert show];
    
}

+ (NSString *) formatDateFromString: (NSString *) dateString patternIN: (NSString *) patternIN patternOUT: (NSString *) patternOUT
{
    // convert to date
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    // ignore +11 and use timezone name instead of seconds from gmt
    [dateFormat setDateFormat:patternIN];
    NSDate *date = [dateFormat dateFromString:dateString];
    
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:patternOUT];
    NSString *formatDate = [dateFormat2 stringFromDate:date];
    return formatDate;
}

+ (NSString *) formatDateFromString: (NSString *) dateString pattern: (NSString *) pattern
{
    return [self formatDateFromString:dateString patternIN:@"yyyy-MM-dd HH:mm:ss" patternOUT:pattern];
}

+ (NSString *) formatDateFromDate: (NSDate *) date pattern: (NSString *) pattern
{
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:pattern];
    NSString *formatDate = [dateFormat2 stringFromDate:date];
    return formatDate;
}

+ (NSString *) getCurrentDate
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormat stringFromDate:today];
    return dateString;
}

// Método que devuelve la duración formateada de la forma hh:mm:ss, desde el valor en segundos totales que le llega.
+ (NSString *) formatTime:(NSString *)time
{
    return [self formatTimeFromFloat:[time floatValue]];
}

+ (NSString *) formatTimeFromFloat:(float)time
{
    
    int segundos = time;
    int horas;
    int minutos;
    if (segundos >= 3600) {
        horas = segundos / 3600;
        segundos = fmod(segundos, 3600);
    } else {
        horas = 0;
    }
    
    if (segundos >= 60) {
        minutos = segundos / 60;
        segundos = fmod(segundos, 60);
    } else {
        minutos = 0;
    }
    
    return [NSString stringWithFormat:@"%02i:%02i:%02i",horas,minutos,segundos];
}

+ (void)drawRoute:(TCDirectionsRoute *)route onMap:(GMSMapView *)mapView
{
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:route.overviewPath];
    polyline.strokeWidth = 10.0f;
    polyline.map = mapView;
}

+ (void) drawRouteWaypoints:(GMSMapView *) mapView coordenadas:(NSDictionary *) coordenadas
{

    GMSMutablePath *path = [GMSMutablePath path];
    NSArray *arr = [NSJSONSerialization JSONObjectWithData:[[NSString stringWithFormat:@"%@", coordenadas] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];
    
    for (NSDictionary * coordenada in arr) {
        NSString *lat = [coordenada objectForKey:TAG_COORDENADAS_LATITUDE];
        NSString *lon = [coordenada objectForKey:TAG_COORDENADAS_LONGITUDE];
        
        [path addCoordinate:CLLocationCoordinate2DMake([lat floatValue], [lon floatValue])];
    }
    
    GMSPolyline *line = [GMSPolyline polylineWithPath:path];
    line.strokeWidth = 10.0f;
    line.strokeColor = [UIColor blueColor];
    line.map = mapView;
    
    GMSCoordinateBounds *coordinateBounds = [[GMSCoordinateBounds alloc] initWithPath:path];
    
    [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:coordinateBounds]];
    
    [mapView setUserInteractionEnabled:FALSE];
}

+ (BOOL) isStringEmpty: (NSString *) string
{
    return string == Nil || [string length] == 0 || [string isEqualToString:@""];
}

+ (void) savePreferenciasUsuario: (NSString *) dni nombre: (NSString*) nombre apellido: (NSString *) apellido apellido2:(NSString *) apellido2 fechaNac:(NSString *) fechaNac telefono: (NSString *) telefono email: (NSString *) email direccion: (NSString *) direccion municipio: (NSString *) municipio provincia: (NSString *) provincia codPostal: (NSString *) codPostal tipo: (NSString *) tipo unidadesMedida: (NSString *) unidadesMedida temperatura: (NSString *) temperatura saveSesion: (NSString *) saveSesion idAuth:(NSString *) idAuth
{
    // Guardar información de usuario en las preferencias de la aplicación
    if (![self isStringEmpty:nombre]) {
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_DNI value:dni];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_EMAIL value:email];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_NOMBRE value:nombre];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_APELLIDO value:apellido];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_APELLIDO2 value:apellido2];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_FECHA_NAC value:fechaNac];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_TELEFONO value:telefono];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_DIRECCION value:direccion];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_MUNICIPIO value:municipio];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_PROVINCIA value:provincia];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_COD_POSTAL value:codPostal];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_TIPO value:tipo];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_ESCALA_UNIDADES value:unidadesMedida];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_UNIDADES_TEMPERATURA value:temperatura];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_SAVE_SESION value:saveSesion];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_ID_AUTH value:idAuth];
    
    NSInteger showLeyendaMapa = [PreferenciasUsuarios getIntInPreferences:PREFERENCIA_LEYENDA_MAPA];
    // Comprobar si existe valor
    if (showLeyendaMapa == 0) {
        [PreferenciasUsuarios saveIntegerInPreferences:PREFERENCIA_LEYENDA_MAPA value:1];
        [PreferenciasUsuarios saveIntegerInPreferences:PREFERENCIA_CONTADOR_ACCESOS value:0];
    }
    }
}

+ (void) savePreferenciasTutorUsuario: (NSString *) dni nombre: (NSString*) nombre apellido: (NSString *) apellido apellido2:(NSString *) apellido2
{
    // Guardar información del tutor del usuario en las preferencias de la aplicación
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_DNI_TUTOR value:dni];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_NOMBRE_TUTOR value:nombre];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_APELLIDO_TUTOR value:apellido];
    [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_APELLIDO2_TUTOR value:apellido2];
    
}

+ (void) removePreferenciasUsuario
{
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_DNI];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_EMAIL];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_NOMBRE];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_APELLIDO];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_APELLIDO2];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_DNI_TUTOR];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_NOMBRE_TUTOR];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_APELLIDO_TUTOR];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_APELLIDO2_TUTOR];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_FECHA_NAC];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_DIRECCION];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_MUNICIPIO];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_PROVINCIA];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_COD_POSTAL];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_TIPO];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_ESCALA_UNIDADES];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_UNIDADES_TEMPERATURA];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_SAVE_SESION];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_CONTADOR_ACCESOS];
    [PreferenciasUsuarios removeObjectInPreferences:PREFERENCIA_ID_AUTH];
}

+ (NSString *) letraDNI: (NSString *) dni
{
    NSArray * letras = @[@"T", @"R", @"W", @"A", @"G", @"M", @"Y", @"F", @"P", @"D", @"X", @"B", @"N", @"J", @"Z", @"S", @"Q", @"V", @"H", @"L", @"C", @"K", @"E"];
    
    return [letras objectAtIndex:[dni integerValue] % 23];
}

+ (BOOL) validateNif: (NSString *) nif
{
    BOOL result = false;
    if ([nif length] == 9)
    {
        NSString * letraOK = [self letraDNI:[nif substringWithRange:NSMakeRange(0, 8)]];
        NSString * letraIN = [nif substringFromIndex:[nif length] - 1];
        
        if ([letraOK isEqualToString:letraIN]) {
            result = true;
        } else {
            result = false;
        }
    } else {
        result = false;
    }
    
    return result;
}

// Validar NIF
+ (BOOL) isNifValido: (NSString *) nifIN
{
    BOOL resultado = false;
    if (nifIN.length == 9) {
            resultado = [self validarNIF:nifIN];
        }
    
    return resultado;
}

+ (BOOL) validarNIF: (NSString *) nifIN
{
    NSCharacterSet *numbersSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
    NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZ"];
    numbersSet = [numbersSet invertedSet];
    charSet = [charSet invertedSet];
    NSString * nif = [nifIN uppercaseString];
    NSRange rangeNumber = [[nif substringWithRange:NSMakeRange(0, nif.length - 1)] rangeOfCharacterFromSet:numbersSet];
    NSRange rangeChar = [[nif substringFromIndex:nif.length - 1] rangeOfCharacterFromSet:charSet];
    
    if (rangeNumber.location == NSNotFound && rangeChar.location == NSNotFound) {
        NSString * letraCalculada = [self letraDNI:[nif substringWithRange:NSMakeRange(0, nif.length - 1)]];
        NSString * letraIN = [nif substringFromIndex:[nif length] - 1];
        if ([letraCalculada isEqualToString:letraIN]) {
            return true;
        }
    }
    
    return false;

}

// Validar CIF
+ (BOOL)isCifValido: (NSString *) cif
{
    BOOL resultado = false;
    
    NSString * vCif = [cif stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    int suma = 0;
    int contador = 0;
    int temporal = 0;
    int codigoControl = 0;
    NSString * cadenaTemporal;
    NSString * valoresCif = @"ABCDEFGHJKLMNPQRSUVW";
    NSString * letraControlCIF = @"0123456789";
    NSString * letraSociedadNumerica = @"KLMNPQRSW";
    NSString * primeraLetra;
    NSString * ultimaLetra;
        
    // Comprueba la longitud correcta del CIF.
    if (vCif.length != 9)
        return false;
    
    NSCharacterSet *letrasValidas = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"];
    letrasValidas = [letrasValidas invertedSet];
    
    NSRange rangeChar = [cif rangeOfCharacterFromSet:letrasValidas];
    
    // Si encuentra algún caracter que no sea una letra o un número, el cif no es válido.
    if (rangeChar.location != NSNotFound)
        return false;
        
    // Convierte a mayúsculas la cadena.
    vCif = [vCif uppercaseString];
        
    // Obtiene la primera letra (letra de la sociedad) y la última letra del CIF (letra de control).
    primeraLetra = [vCif substringWithRange:NSMakeRange(0, 1)];
    
    // Obtiene la última letra del CIF, para comprobar si es válida.
    ultimaLetra = [vCif substringFromIndex:8];
        
    // Comprueba si la primera letra es válida.
    if ([valoresCif rangeOfString:primeraLetra].location == NSNotFound)
        return false;
        
    // Obtiene el código de control.
    // Sumamos las cifras pares
    suma = [[vCif substringWithRange:NSMakeRange(2, 1)] intValue] + [[vCif substringWithRange:NSMakeRange(4, 1)] intValue] + [[vCif substringWithRange:NSMakeRange(6, 1)] intValue];
        
    // Ahora cada cifra impar la multiplicamos por dos y sumamos las cifras del resultado.
    for (contador = 1; contador < 8; contador = contador + 2) {
        // Multiplica por 2
        temporal = [[vCif substringWithRange:NSMakeRange(contador, 1)] intValue] * 2;
        
        // Suma los digitos.
        // Diferencia si tiene una cifra, por ejemplo: 8 = 8 o si tiene varias, por ejemplo: 16 -> 6 + 1 = 7
        if (temporal < 10)
            suma = suma + temporal;
        else {
            cadenaTemporal = [NSString stringWithFormat:@"%ld", (long)temporal];
            suma = suma + [[cadenaTemporal substringWithRange:NSMakeRange(0, 1)] intValue] + [[cadenaTemporal substringWithRange:NSMakeRange(1, 1)] intValue];
        }
    }
        
    // Obtiene las unidades de la suma y se las resta a 10, para obtener el dìgito de control.
    codigoControl = ((10 - (suma % 10)) % 10);
        
    // Si la letra es K, L, M, N, P, Q ó S entonces al codigo de control le suma 64 y obtengo su ASCII para ver si coincide con la ultima
    // letra del cif.
    if ([letraSociedadNumerica rangeOfString:primeraLetra].location != NSNotFound) {
        // Obtiene el código ASCII asociado, al sumar 64 al código de control.
        if (codigoControl == 0)
            codigoControl = 10;
        codigoControl = codigoControl + 64;
        int asciiCode = [[NSString stringWithFormat:@"%ld", (long)codigoControl] characterAtIndex:0];
        //ascii[0] = (Integer.valueOf(codigoControl)).byteValue();
            
        // El último dìgito tiene que coincidir con el dìgito de control obtenido
        resultado = [ultimaLetra isEqualToString:[NSString stringWithFormat:@"%ld", (long) asciiCode]];
        //resultado = (ultimaLetra.equals(new String(ascii)));
    } else {
        // Para el resto de letras de comienzo de CIF el último dìgito debe ser numérico y coincidir con el código de control.
        resultado = (codigoControl == [letraControlCIF rangeOfString:ultimaLetra].location);
    }
    
    return resultado;
}

// Validar TARJETA DE RESIDENTE
+ (BOOL) isTarjetaDeResidenciaValida: (NSString *) cadena
{
    int longitud = 0;
    BOOL correcto = true;
    NSString * nif;
    NSString * primerCaracter;
    NSString * segundoCaracter;
    NSString * tercerCaracter;
    NSString * valoresPrimerCaracter = @"KLTXYZ";
    NSString * letras = @"ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    
    // Valida la longitud de la cadena.
    longitud = (int) cadena.length;
    if (longitud == 0) {
        return false;
    } else if (!(longitud == 11 || longitud == 9)) {
        return false;
    }
    
    primerCaracter = [cadena substringWithRange:NSMakeRange(0, 1)];
    
    if (longitud == 11) {
        segundoCaracter = [[cadena substringWithRange:NSMakeRange(1, 1)] uppercaseString];
        tercerCaracter = [[cadena substringWithRange:NSMakeRange(2, 1)] uppercaseString];
        nif = [[cadena substringWithRange:NSMakeRange(2, 9)] uppercaseString];
        
        if ([valoresPrimerCaracter rangeOfString:primerCaracter].location == -1)
            correcto = false;
        else if ([letras rangeOfString:segundoCaracter].location == -1)
            correcto = false;
        else if ([tercerCaracter isEqualToString:@"00"])
            correcto = false;
        else
            correcto = [self isNifValido:nif];
    }
    
    if (longitud == 9) {
        if ([primerCaracter isEqualToString:@"X"]) {
            nif = [NSString stringWithFormat:@"0%@", [[cadena substringWithRange:NSMakeRange(1, 8)] uppercaseString]];
            correcto = [self isNifValido:nif];
        } else if ([primerCaracter isEqualToString:@"Y"]) {
            nif = [NSString stringWithFormat:@"1%@", [[cadena substringWithRange:NSMakeRange(1, 8)] uppercaseString]];
            correcto = [self isNifValido:nif];
        } else if ([primerCaracter isEqualToString:@"Z"]) {
            nif = [NSString stringWithFormat:@"2%@", [[cadena substringWithRange:NSMakeRange(1, 8)] uppercaseString]];
            correcto = [self isNifValido:nif];
        } else {
            correcto = false;
        }
    }
    return correcto;
}

// Validar NIE
+ (BOOL) isNIEValido:(NSString *) nie {
    
    BOOL resultado = false;
    
    if (nie.length == 9) {
        NSString * nie1 = [nie uppercaseString];
        NSString * pre = [nie1 substringWithRange:NSMakeRange(0, 1)];
        if ([pre isEqualToString:@"X"] || [pre isEqualToString:@"Y"] || [pre isEqualToString:@"Z"]) {
            resultado = [self validarNIF:[NSString stringWithFormat:@"0%@", [nie1 substringWithRange:NSMakeRange(1, nie1.length - 1)]]];
        }
    }
    return resultado;
}

// Generates alpha-numeric-random string
+ (NSString *) generarCodigoRecogida:(int)len {
    static NSString *letters = @"abcdefghijklmnopqrstuvwxyz0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
    return randomString;
}

+ (BOOL) isUsuarioMayorDe: (NSInteger) edadPermitida birthday: (NSDate*) birthdate
{
    BOOL resultado = false;
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *compoBirthday = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:birthdate];
    
    NSDate *now = [NSDate date];
    NSDateComponents *compoNow = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now];

    //NSDate *today10am = [calendar dateFromComponents:components];
    
    if (compoNow.year - compoBirthday.year > edadPermitida)
    {
        resultado = true;
    } else if (compoNow.year - compoBirthday.year == edadPermitida) {
        if (compoNow.month > compoBirthday.month) {
            resultado = true;
        } else if (compoNow.month == compoBirthday.month)
        {
            if (compoNow.day >= compoBirthday.day)
            {
                resultado = true;
            } else {
                resultado = false;
            }
        } else {
            resultado = false;
        }
    } else {
        resultado = false;
    }
    return resultado;
}

+ (BOOL) isUsuarioMenorDe: (NSInteger) edadPermitida birthday: (NSDate*) birthdate
{
    BOOL resultado = false;
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *compoBirthday = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:birthdate];
    
    NSDate *now = [NSDate date];
    NSDateComponents *compoNow = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now];
    
    //NSDate *today10am = [calendar dateFromComponents:components];
    
    if (compoNow.year - compoBirthday.year < edadPermitida)
    {
        resultado = true;
    } else if (compoNow.year - compoBirthday.year == edadPermitida) {
        if (compoNow.month < compoBirthday.month) {
            resultado = true;
        } else if (compoNow.month == compoBirthday.month)
        {
            if (compoNow.day < compoBirthday.day)
            {
                resultado = true;
            } else {
                resultado = false;
            }
        } else {
            resultado = false;
        }
    } else {
        resultado = false;
    }
    return resultado;
}


@end
