//
//  ComunTablasViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 04/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD.h>

#import "ComunObject.h"
#import "DetalleRutaViewController.h"
#import "PageViewController.h"
#import "RutaObject.h"
#import "PreferenciasUsuarios.h"


@interface ComunTablasViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak) NSDictionary *rutasData;
@property(nonatomic, strong) NSMutableArray *rutas;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *labelHuella;
@property (weak, nonatomic) IBOutlet UILabel *labelCalorias;
@property (weak, nonatomic) IBOutlet UILabel *labelKM;
@property (weak, nonatomic) IBOutlet UILabel *labelUsos;
@property (weak, nonatomic) IBOutlet UILabel *labelDuracion;
@property (weak, nonatomic) IBOutlet UILabel *literalDistancia;

@property NSString * tipoDato;

- (void)getRutasUsuario;
@end
