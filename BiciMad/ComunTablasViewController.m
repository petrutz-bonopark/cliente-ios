//
//  ComunTablasViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 04/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "ComunTablasViewController.h"

@interface ComunTablasViewController ()

@end

float kmTotales = 0;
float caloriasTotales = 0;
float huellaCO2Total = 0;
float duracionTotal = 0;

@implementation ComunTablasViewController

NSString * escalaUnidadesUsuario;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    escalaUnidadesUsuario = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_ESCALA_UNIDADES];
    
    if ([escalaUnidadesUsuario isEqualToString:@"mi"]) {
        _literalDistancia.text = @"mi";
    } else {
        _literalDistancia.text = @"km";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getRutasUsuario
{
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    progressHUD.dimBackground = YES;
    progressHUD.labelText = @"Obteniendo historial...";
    
    // Todos los toques de pantalla deshabilitan el progressHUD
    progressHUD.userInteractionEnabled = YES;
    
    NSDictionary *myJson=@{TAG_DNI: [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DNI],
                           TAG_ID_AUTH: [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_ID_AUTH],
                           };
    
    
    NSString *getRuta = [NSString  stringWithFormat:@"%@%@?format=json", BaseURLString, url_get_ruta_usuario];
    NSURL *url = [NSURL  URLWithString:getRuta];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFJSONParameterEncoding;
    NSDictionary *params = myJson ;
    
    [httpClient postPath:url_save_ruta parameters:params
                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                     // Print the response body in text
                     NSLog(@"Write to DB: Success");
                     NSError *error ;
                     NSDictionary* jsonFromData = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                     NSLog(@"Return string: %@", jsonFromData);
                     
                     self.rutas = [[NSMutableArray alloc] init];
                     duracionTotal = 0;
                     kmTotales = 0;
                     caloriasTotales = 0;
                     huellaCO2Total = 0;
                     
                     NSDictionary *rutaJSON = [jsonFromData objectForKey:@"rutas"];
                     for (NSDictionary *ruta in rutaJSON) {
                         
                         NSString *fecha = [ComunObject formatDateFromString:[ruta objectForKey:@"fecha"] pattern:@"dd-MM-yyyy HH:mm:ss"];
                         NSString *duracionRuta = [ruta objectForKey:@"duracion"];
                         duracionTotal += [[ruta objectForKey:@"duracion"] floatValue];
                         NSString *distanciaRuta = [ruta objectForKey:@"km"];
                         kmTotales += [distanciaRuta floatValue];
                         NSString *velocidadMediaRuta = [ruta objectForKey:@"velocidad_media"];
                         NSString *parcialMedioRuta = [ruta objectForKey:@"parcial_medio"];
                         NSString *caloriasRuta = [ruta objectForKey:@"calorias_consumidas"];
                         caloriasTotales += [caloriasRuta floatValue];
                         NSString *huellaCO2Ruta = [ruta objectForKey:@"huella_CO2"];
                         huellaCO2Total += [huellaCO2Ruta floatValue];
                         NSString *velocidadMaxRuta = [ruta objectForKey:@"velocidad_max"];
                         NSString *elevacionRuta = [ruta objectForKey:@"elevacion_ruta"];
                         NSDictionary *coordenadasRuta = [ruta objectForKey:@"coordenadas_ruta"];
                         
                         RutaObject *rutaObject = [[RutaObject alloc] initWithText:fecha duracion:duracionRuta distancia:distanciaRuta velocidadMedia:velocidadMediaRuta parcial:parcialMedioRuta calorias:caloriasRuta huellaCO2:huellaCO2Ruta velocidadMax:velocidadMaxRuta elevacion:elevacionRuta coordenadas:coordenadasRuta];
                         [self.rutas addObject:rutaObject];
                     }
                     
                     [self.tableView reloadData];
                     
                     float distanciaTotal;
                     if ([escalaUnidadesUsuario isEqualToString:@"mi"]) {
                         distanciaTotal = kmTotales * millasXKM;
                     } else {
                         distanciaTotal = kmTotales;
                     }
                     self.labelDuracion.text = [ComunObject formatTimeFromFloat:duracionTotal];
                     self.labelKM.text = [NSString stringWithFormat:@"%.02f", distanciaTotal];
                     self.labelCalorias.text = [NSString stringWithFormat:@"%.02f", caloriasTotales];
                     self.labelHuella.text = [NSString stringWithFormat:@"%.02f", huellaCO2Total];
                     self.labelUsos.text = [NSString stringWithFormat:@"%lu", (unsigned long)[rutaJSON count]];
                     
                     [progressHUD hide:YES];
                     
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Write to DB: Fail");
                     [progressHUD hide:YES];
                     
                     [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"LABEL_ERROR_CONNECTION"];
                 }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.rutas count];
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    // The header for the section is the region name -- get this from the region at the section index.
//    return @"Mi historial";
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"rutaCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    RutaObject *ruta = [self.rutas objectAtIndex:indexPath.row];
    
    UILabel *tituloLabel = (UILabel *)[cell viewWithTag:100];
    tituloLabel.text = [NSString stringWithFormat:@"%@", ruta.fechaRuta];
    
    UILabel *detalleLabel = (UILabel *)[cell viewWithTag:101];
    if ([self.tipoDato isEqualToString:TIPO_DATO_KM]) {
        float distanciaTotal;
        if ([escalaUnidadesUsuario isEqualToString:@"mi"]) {
            distanciaTotal = [ruta.distanciaRuta floatValue] * millasXKM;
        } else {
            distanciaTotal = [ruta.distanciaRuta floatValue];
        }
        detalleLabel.text = [NSString stringWithFormat:@"%.02f %@", distanciaTotal, _literalDistancia.text];
    } else if ([self.tipoDato isEqualToString:TIPO_DATO_CAL]) {
        detalleLabel.text = [NSString stringWithFormat:@"%@ cal", ruta.caloriasRuta];
    } else if ([self.tipoDato isEqualToString:TIPO_DATO_HUE]) {
        detalleLabel.text = [NSString stringWithFormat:@"%@ kg", ruta.huellaCO2Ruta];
    }

    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"DetalleRuta"]) {
        RutaObject *ruta = [self.rutas objectAtIndex:self.tableView.indexPathForSelectedRow.row];
        
        PageViewController *detalleViewController = (PageViewController *) [segue destinationViewController];
        [detalleViewController setMyRuta:ruta];
    }
}

- (IBAction)actualizarHistorialRutas:(id)sender {
    [self getRutasUsuario];
}

@end
