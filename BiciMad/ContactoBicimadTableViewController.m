//
//  ContactoBicimadTableViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 04/06/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "ContactoBicimadTableViewController.h"

@interface ContactoBicimadTableViewController ()

@end

@implementation ContactoBicimadTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) openUrlSafari
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.madrid.es/portales/munimadrid/es/Inicio/Ayuntamiento/Contactar/Sugerencias-y-reclamaciones?vgnextfmt=default&vgnextchannel=5eadc1ab4fd86210VgnVCM2000000c205a0aRCRD"]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    return cell;
}

@end
