//
//  CuentaUsuarioViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 04/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD.h>
#import "TableViewCuentaUsuarioViewController.h"
#import "ComunObject.h"

@interface CuentaUsuarioViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *buttonCerrarSesion;

@end
