//
//  CuentaUsuarioViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 04/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "CuentaUsuarioViewController.h"

@interface CuentaUsuarioViewController ()

@end

@implementation CuentaUsuarioViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Prueba de notificación
//    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
//    
//    localNotification.fireDate =[NSDate dateWithTimeIntervalSinceNow:10];
//    
//    localNotification.alertBody = @"Factura electrónica del pago del abono con recibo 2014XXXX";
//    
//    localNotification.timeZone = [NSTimeZone defaultTimeZone];
//    
//    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
