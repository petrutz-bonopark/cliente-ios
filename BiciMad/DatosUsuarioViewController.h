//
//  DatosUsuarioViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 31/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFHTTPClient.h>
#import "ComunObject.h"
#import "DateTimePicker.h"

@protocol CambioDatosDelegate <NSObject>

- (void)updatedatosUsuario;

@end

@interface DatosUsuarioViewController : UITableViewController

@property (nonatomic, weak) id<CambioDatosDelegate> delegate;

@property (strong, nonatomic) IBOutlet UILabel *nombreUsuario;
@property (strong, nonatomic) IBOutlet UILabel *emailUsuario;
@property (strong, nonatomic) IBOutlet UITextField *fechaNacUsuario;
@property (strong, nonatomic) IBOutlet UILabel *direccionUsuario;
@property (strong, nonatomic) IBOutlet UILabel *numTelefonoUsuario;


@property DateTimePicker * datePicker;

@end
