//
//  DatosUsuarioViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 31/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "DatosUsuarioViewController.h"

@interface DatosUsuarioViewController ()

@end

@implementation DatosUsuarioViewController

@synthesize datePicker;

NSDate *selectedDate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidAppear:(BOOL)animated
{
    self.nombreUsuario.text = [NSString stringWithFormat:@"%@ %@ %@", [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_NOMBRE],
                               [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO],
                               [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO2]
                               ];
    self.emailUsuario.text = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_EMAIL];
    self.direccionUsuario.text = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DIRECCION];
    self.numTelefonoUsuario.text = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_TELEFONO];
    
    self.fechaNacUsuario.text = [ComunObject formatDateFromString:[PreferenciasUsuarios getStringInPreferences:PREFERENCIA_FECHA_NAC]
                                                          patternIN:@"yyyy-MM-dd" patternOUT:@"dd-MM-yyyy"];

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController) {
        NSLog(@"Back event: Guardar datos");
        [self actualizarDatosUsuario];
        
        [self.delegate updatedatosUsuario];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    datePicker = [[DateTimePicker alloc] initWithFrame:CGRectMake(0, screenHeight/2 - 35, screenWidth, screenHeight/2 + 35)];
    
    [datePicker addTargetForDoneButton:self action:@selector(donePressed)];
    [self.view addSubview:datePicker];
    datePicker.hidden =YES;
    [datePicker setMode:UIDatePickerModeDate];
    [datePicker.picker addTarget:self action:@selector(pickerChanged) forControlEvents:UIControlEventValueChanged];
    
    datePicker.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)pickerChanged {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd-MM-yyyy";
    
    selectedDate = datePicker.picker.date;
    self.fechaNacUsuario.text = [dateFormatter stringFromDate:selectedDate];
    
}

- (IBAction)fechaNacimientoDidBegingEditing:(UITextField *) sender {
    
    [self.fechaNacUsuario resignFirstResponder];
    [sender resignFirstResponder];
    datePicker.hidden = NO;
}

-(void)donePressed {
    datePicker.hidden = YES;
    if (self.fechaNacUsuario.text.length > 0) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        
        selectedDate = datePicker.picker.date;
        [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_FECHA_NAC value:
         [dateFormatter stringFromDate:selectedDate]];
    }
}

- (void) actualizarDatosUsuario
{
    
    NSDictionary *myJson=@{TAG_DNI : [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DNI],
                           TAG_ID_AUTH : [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_ID_AUTH],
                           TAG_NAME : [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_NOMBRE],
                           TAG_APELLIDO1 : [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO],
                           TAG_APELLIDO2 : [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO2],
                           TAG_EMAIL : [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_EMAIL],
                           TAG_DIRECCION : [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DIRECCION],
                           TAG_MUNICIPIO : [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_MUNICIPIO],
                           TAG_PROVINCIA : [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_PROVINCIA],
                           TAG_COD_POSTAL : [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_COD_POSTAL],
                           TAG_TELEFONO : [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_TELEFONO],
                           TAG_FECHA_NACIMIENTO : [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_FECHA_NAC]};
    
    NSURL *url = [NSURL URLWithString:BaseURLString];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFJSONParameterEncoding;
    NSDictionary *params = myJson ;
    
    [httpClient postPath:url_update_user parameters:params
                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                     // Print the response body in text
                     NSLog(@"Write to DB: Success");
                     NSError *error ;
                     NSDictionary* jsonFromData = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                     NSLog(@"Return string: %@", jsonFromData);
                     
//                     NSString *success = [jsonFromData objectForKey:TAG_SUCCESS];
//                     NSString *message = [jsonFromData objectForKey:TAG_MESSAGE];
                     
//                     if ([success integerValue] == 1) {
//                         
//                     } else {
//                         [ComunObject alertError:message];
//                     }
                     
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Write to DB: Fail");
                     
                     [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"LABEL_ERROR_CONNECTION"];
                 }];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{	
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}

@end
