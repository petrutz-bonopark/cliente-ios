//
//  DetalleRutaViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 10/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ComunObject.h"
#import "RutaObject.h"

@interface DetalleRutaViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UILabel *fechaRuta;
@property (weak, nonatomic) IBOutlet UILabel *duracionRuta;
@property (weak, nonatomic) IBOutlet UILabel *distanciaRuta;
@property (weak, nonatomic) IBOutlet UILabel *velocidadMediaRuta;
@property (weak, nonatomic) IBOutlet UILabel *parcialMedioRuta;
@property (weak, nonatomic) IBOutlet UILabel *caloriasRuta;
@property (weak, nonatomic) IBOutlet UILabel *huellaCO2Ruta;
@property (weak, nonatomic) IBOutlet UILabel *velocidadMaxRuta;
@property (weak, nonatomic) IBOutlet UILabel *elevacionRuta;

@property NSUInteger pageIndex;

@property (strong, nonatomic) RutaObject *rutaObject;

- (void) setMyRuta: (RutaObject *) ruta;
@end
