//
//  DetalleRutaViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 10/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "DetalleRutaViewController.h"

@interface DetalleRutaViewController ()

@end

@implementation DetalleRutaViewController

@synthesize rutaObject;

- (void) setMyRuta:(RutaObject *)ruta {

    if (ruta) {
        rutaObject = ruta;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _fechaRuta.text = rutaObject.fechaRuta;
    _duracionRuta.text = [ComunObject formatTime:rutaObject.duracionRuta];
    _distanciaRuta.text = [NSString stringWithFormat:@"%@ km", rutaObject.distanciaRuta];
    _velocidadMediaRuta.text = [NSString stringWithFormat:@"%@ km/h", rutaObject.velocidadMediaRuta];
    _parcialMedioRuta.text = rutaObject.parcialMedioRuta;
    _caloriasRuta.text = [NSString stringWithFormat:@"%@ cal", rutaObject.caloriasRuta];
    _huellaCO2Ruta.text = [NSString stringWithFormat:@"%@ kg", rutaObject.huellaCO2Ruta];
    _velocidadMaxRuta.text = [NSString stringWithFormat:@"%@ km/h", rutaObject.velocidadMaxRuta];
    _elevacionRuta.text = rutaObject.elevacionRuta;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    return cell;
}

@end
