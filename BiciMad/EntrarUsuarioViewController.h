//
//  EntrarUsuarioViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 28/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD.h>
#import "CustomIOS7AlertView.h"
#import "ComunObject.h"
#import "MainVC.h"

@interface EntrarUsuarioViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *dni;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UISwitch *switchRecordar;
@property (weak, nonatomic) IBOutlet UIButton *buttonAbrirSesion;
@property (weak, nonatomic) IBOutlet UIButton *buttonCancelar;


- (IBAction)backgroundTap:(id)sender;
- (IBAction) abrirSesionUsuario:(id)sender;

@end
