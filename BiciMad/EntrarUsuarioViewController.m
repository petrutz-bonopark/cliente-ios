//
//  EntrarUsuarioViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 28/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "EntrarUsuarioViewController.h"

@interface EntrarUsuarioViewController ()

@end

@implementation EntrarUsuarioViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self.dni becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) abrirSesionUsuario:(id)sender
{
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    progressHUD.dimBackground = YES;
    progressHUD.labelText = @"Accediendo a tu cuenta...";
    
    NSString * save;
    if ([self.switchRecordar isOn])
    {
        save = @"YES";
    } else {
        save = @"NO";
    }
    NSString * codigoPush = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DEV_TOKEN];
    
    // PARA PROBAR EN EL EMULADOR
    if (codigoPush == nil) {
        codigoPush = @"NO";
    }
    
    NSDictionary *myJson=@{TAG_DNI: self.dni.text,
                           TAG_PASSWORD: self.password.text,
                           TAG_CODIGO_PUSH: codigoPush};
    
    NSURL *url = [NSURL URLWithString:BaseURLString];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFJSONParameterEncoding;
    NSDictionary *params = myJson ;
    
    [httpClient postPath:url_get_user parameters:params
                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                     NSLog(@"Write to DB: Success");
                     NSError *error ;
                     NSDictionary* jsonFromData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                     NSLog(@"Return string: %@", operation.responseString);
                     
                     NSString *success = [jsonFromData objectForKey:TAG_SUCCESS];
                     NSString *message = [jsonFromData objectForKey:TAG_MESSAGE];
                     
                     if ([success integerValue] == 1) {
                         NSDictionary * usuarioAlta = [jsonFromData objectForKey:TAG_USUARIO];
                         
                         for (NSDictionary * usuario in usuarioAlta) {
                             NSString * dni = [usuario objectForKey:TAG_DNI];
                             NSString * idAuth = [usuario objectForKey:TAG_ID_AUTH];
                             NSString * nombre = [usuario objectForKey:TAG_NOMBRE];
                             NSString * apellido = [usuario objectForKey:TAG_APELLIDO1];
                             NSString * apellido2 = [usuario objectForKey:TAG_APELLIDO2];
                             NSString * fechaNac = [usuario objectForKey:TAG_FECHA_NACIMIENTO];
                             NSString * email = [usuario objectForKey:TAG_EMAIL];
                             NSString * telefono = [usuario objectForKey:TAG_TELEFONO];
                             NSString * direccion = [usuario objectForKey:TAG_DIRECCION];
                             NSString * municipio = [usuario objectForKey:TAG_MUNICIPIO];
                             NSString * provincia = [usuario objectForKey:TAG_PROVINCIA];
                             NSString * codPostal = [usuario objectForKey:TAG_COD_POSTAL];
                             [ComunObject savePreferenciasUsuario:dni nombre:nombre apellido:apellido apellido2:apellido2 fechaNac:fechaNac telefono:telefono email:email direccion:direccion municipio:municipio provincia:provincia codPostal:codPostal tipo:@"3" unidadesMedida:@"km" temperatura:@"C" saveSesion:save idAuth:idAuth];
                         }

                         [self performSegueWithIdentifier:@"AbrirSesion" sender:self];
                     } else {
                         [ComunObject alertErrorMessage:@"TITULO_ALERT_ERROR" texto:message];
                     }
                     // Ocultamos el progressHUD
                     [progressHUD hide:YES];
                     
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Write to DB: Fail");
                     [progressHUD hide:YES];
                     
                     [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"LABEL_ERROR_CONNECTION"];
                 }];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.dni) {
        [textField resignFirstResponder];
        [self.password becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return true;
}


- (IBAction)backgroundTap:(id)sender {
    [self.view endEditing:YES];
}
@end
