//
//  GMSEstacionMarker.h
//  BiciMad
//
//  Created by Miguel Asuar on 06/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

@interface GMSEstacionMarker : GMSMarker

@property (nonatomic, weak) NSString * numBasesLibres;

@property (nonatomic, weak) NSString * numBasesOcupadas;

@end
