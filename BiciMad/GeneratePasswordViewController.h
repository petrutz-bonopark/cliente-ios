//
//  GeneratePasswordViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 02/04/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFHTTPClient.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "ComunObject.h"


@interface GeneratePasswordViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *textDni;
@property (strong, nonatomic) IBOutlet UITextField *textEmail;

- (IBAction)generatePassword:(id)sender;

@end
