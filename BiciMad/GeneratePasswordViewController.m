//
//  GeneratePasswordViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 02/04/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "GeneratePasswordViewController.h"

@interface GeneratePasswordViewController ()

@end

@implementation GeneratePasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self.textDni becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)generatePassword:(id)sender {
    if ([ComunObject validateNif:self.textDni.text]
        && self.textEmail.text > 0) {
        
    
        MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        progressHUD.dimBackground = YES;
        progressHUD.labelText = @"Generando nueva contraseña...";
    
        NSDictionary *myJson=@{TAG_DNI : self.textDni.text,
                               TAG_EMAIL : self.textEmail.text};
        
        NSURL *url = [NSURL URLWithString:BaseURLString];
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
        httpClient.parameterEncoding = AFJSONParameterEncoding;
        NSDictionary *params = myJson ;
        
        [httpClient postPath:url_generate_password parameters:params
                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                         // Print the response body in text
                         NSLog(@"Write to DB: Success");
                         NSError *error ;
                         NSDictionary* jsonFromData = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                         NSLog(@"Return string: %@", jsonFromData);
                         
                         //                     NSString *success = [jsonFromData objectForKey:TAG_SUCCESS];
                         //                     NSString *message = [jsonFromData objectForKey:TAG_MESSAGE];
                         
                         //                     if ([success integerValue] == 1) {
                         //
                         //                     } else {
                         //                         [ComunObject alertError:message];
                         //                     }
                         [progressHUD hide:YES];
                         
                         UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Recuperación contraseña en BiciMAD" message:@"Acceda a la app con la contraseña que le hemos enviado al correo" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
                         alert.alertViewStyle = UIAlertViewStyleDefault;
                         alert.tag = 1;
                         
                         [alert show];
                         
                     }
                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                         NSLog(@"Write to DB: Fail");
                         [progressHUD hide:YES];
                         
                         [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"LABEL_ERROR_CONNECTION"];
                         
                     }];
    } else {
        [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"MENS_RELLENAR_DATOS_USUARIO"];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //NSLog(@"Entered: %@",[[alertView textFieldAtIndex:0] text]);
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            [self performSegueWithIdentifier:@"VolverInicioSesion" sender:self];
            
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    if (textField == self.textDni) {
        [textField resignFirstResponder];
        [self.textEmail becomeFirstResponder];
    } else if (textField == self.textEmail) {
        [textField resignFirstResponder];
    }
    
    return NO;
}


@end
