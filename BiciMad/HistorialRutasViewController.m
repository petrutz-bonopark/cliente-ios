//
//  HistorialRutasViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 07/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "HistorialRutasViewController.h"

@interface HistorialRutasViewController ()

@end

@implementation HistorialRutasViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.tipoDato = TIPO_DATO_KM;
    
    [self getRutasUsuario];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
