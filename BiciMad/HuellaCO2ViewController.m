//
//  HuellaCO2ViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 04/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "HuellaCO2ViewController.h"

@interface HuellaCO2ViewController ()

@end

@implementation HuellaCO2ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tipoDato = TIPO_DATO_HUE;
    [self getRutasUsuario];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
