//
//  IncidenciaTableViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 04/06/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComunObject.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking.h>

@interface IncidenciaTableViewController : UITableViewController <UITextFieldDelegate, UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UITextField *nombreUsuario;
@property (strong, nonatomic) IBOutlet UITextField *incidencia;
@property (strong, nonatomic) IBOutlet UITextField *direccion;
@property (strong, nonatomic) IBOutlet UITextField *numEstacion;

@property (strong, nonatomic) IBOutlet UITextView *detalleIncidencia;

@end
