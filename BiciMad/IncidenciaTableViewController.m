//
//  IncidenciaTableViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 04/06/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "IncidenciaTableViewController.h"

@interface IncidenciaTableViewController ()

@end

@implementation IncidenciaTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) enviarIncidencia:(id)sender
{
    if (self.nombreUsuario.text.length > 0
        && self.incidencia.text.length > 0
        && self.detalleIncidencia.text.length > 0
        && self.direccion.text.length > 0
        && self.detalleIncidencia.text.length > 0) {
    
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    progressHUD.dimBackground = YES;
    progressHUD.labelText = @"Registrando incidencia...";
    
    NSDictionary *myJson=@{@"dni_usuario": [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DNI],
                           TAG_EMAIL: [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_EMAIL],
                           TAG_NOMBRE: self.nombreUsuario.text,
                           TAG_NOMBRE_INCIDENCIA: self.incidencia.text,
                           TAG_DIRECCION_INCIDENCIA: self.direccion.text,
                           TAG_ESTACION_INCIDENCIA: self.numEstacion.text,
                           TAG_DETALLE_INCIDENCIA: self.detalleIncidencia.text};
    
    NSURL *url = [NSURL URLWithString:BaseURLString];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFJSONParameterEncoding;
    NSDictionary *params = myJson ;
    
    [httpClient postPath:url_registrar_incidencia parameters:params
                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                     // Print the response body in text
                     NSLog(@"Write to DB: Success");
                     NSError *error;
                     NSDictionary* jsonFromData = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                     NSLog(@"Return string: %@", jsonFromData);
                                          
                     [ComunObject alertError:@"TITULO_INCIDENCIA" idTexto:@"MENSAJE_INCIDENCIA"];
                     
                     // Ocultamos el progressHUD
                     [progressHUD hide:YES];
                     
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Write to DB: Fail");
                     [progressHUD hide:YES];
                     
                     [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"LABEL_ERROR_CONNECTION"];
                 }];
    } else {
        [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"MENS_RELLENAR_DATOS_USUARIO"];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    return cell;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.nombreUsuario) {
        [textField resignFirstResponder];
        [self.incidencia becomeFirstResponder];
    } else if (textField == self.incidencia) {
        [textField resignFirstResponder];
        [self.direccion becomeFirstResponder];
    } else if (textField == self.direccion) {
        [textField resignFirstResponder];
        [self.numEstacion becomeFirstResponder];
    } else if (textField == self.numEstacion) {
        [textField resignFirstResponder];
        [self.detalleIncidencia becomeFirstResponder];
    }
    return false;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView == self.detalleIncidencia) {
        [textView resignFirstResponder];
    }
}

@end
