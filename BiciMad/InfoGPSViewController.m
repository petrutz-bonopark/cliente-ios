//
//  InfoGPSViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 06/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "InfoGPSViewController.h"

@interface InfoGPSViewController ()

@end

@implementation InfoGPSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [PSLocationManager sharedLocationManager].delegate = self;
    [[PSLocationManager sharedLocationManager] prepLocationUpdates];
    // [[PSLocationManager sharedLocationManager] startLocationUpdates];
    
    indiceCalorico = [NSDictionary dictionaryWithObjectsAndKeys:  @"0.0295", @"13.0",
                                     @"0.0355", @"16.0",
                                     @"0.0426", @"19.0",
                                     @"0.0512", @"22.5",
                                     @"0.0561", @"24.0",
                                     @"0.0615", @"25.5",
                                     @"0.0675", @"27.0",
                                     @"0.0740", @"29.0",
                                     @"0.0811", @"30.5",
                                     @"0.0891", @"32.0",
                                     @"0.0975", @"33.5",
                                     @"0.1173", @"37.0",
                                     @"0.1411", @"40.0", nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
