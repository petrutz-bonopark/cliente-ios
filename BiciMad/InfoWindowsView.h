//
//  InfoWindowsView.h
//  BiciMad
//
//  Created by Miguel Asuar on 02/04/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoWindowsView : UIView

@property (strong, nonatomic) IBOutlet UILabel *nombre;
@property (strong, nonatomic) IBOutlet UILabel *direccion;

@end
