//
//  InfoWindowsView.m
//  BiciMad
//
//  Created by Miguel Asuar on 02/04/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "InfoWindowsView.h"

@implementation InfoWindowsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
