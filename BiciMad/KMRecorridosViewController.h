//
//  KMRecorridosViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 04/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComunTablasViewController.h"

@interface KMRecorridosViewController : ComunTablasViewController

@property (weak, nonatomic) IBOutlet UILabel *labelKm;
@end
