//
//  KMRecorridosViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 04/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "KMRecorridosViewController.h"

@interface KMRecorridosViewController ()

@end

@implementation KMRecorridosViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tipoDato = TIPO_DATO_KM;
    
    [self getRutasUsuario];
    
    //self.labelKm.text = [NSString stringWithFormat:@"%f", kmTotales];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
