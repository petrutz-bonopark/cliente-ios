//
//  LeftMenuVC.h
//  AMSlideMenu
//
//  Created by Artur Mkrtchyan on 12/24/13.
//  Copyright (c) 2013 Artur Mkrtchyan. All rights reserved.
//

#import "AMSlideMenuLeftTableViewController.h"
#import "PreferenciasUsuarios.h"
#import "ComunObject.h"
#import <AFNetworking/AFNetworking.h>

@interface LeftMenuVC : AMSlideMenuLeftTableViewController

@property (weak, nonatomic) IBOutlet UILabel *nombreUsuario;
@property (strong, nonatomic) IBOutlet UILabel *saldoActual;

@end
