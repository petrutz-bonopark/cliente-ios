//
//  TCEstacion.h
//  BiciMad
//
//  Created by Miguel Asuar on 04/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface TCEstacion : NSObject

@property (nonatomic, strong) NSString *idEstacion;
/**
 * The name of the place.
 */
@property (nonatomic, strong) NSString *nombre;

/**
 * Short-form address of the place.
 */
@property (nonatomic, strong) NSString *direccion;

/**
 * Activate control.
 */
@property (nonatomic, strong) NSString *activo;

/**
 * The place's coordinates.
 */
@property (nonatomic, assign) CLLocationCoordinate2D location;

@property (nonatomic, strong) NSString * status;

@property (nonatomic, strong) NSString * luz;

@property (nonatomic, strong) NSString * numBases;

@property (nonatomic, strong) NSString * basesActivadas;

@property (nonatomic, strong) NSString * porcentajeOcupacion;


@end
