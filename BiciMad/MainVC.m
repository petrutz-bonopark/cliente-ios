#import "MainVC.h"

@interface MainVC ()

@end

@implementation MainVC

- (void)viewDidLoad
{
    [super viewDidLoad];

}

/*----------------------------------------------------*/
#pragma mark - Overriden Methods -
/*----------------------------------------------------*/

- (NSString *)segueIdentifierForIndexPathInLeftMenu:(NSIndexPath *)indexPath
{
    NSString *identifier = @"";
    switch (indexPath.row) {
        case 0:
            if (indexPath.section == 0) {
                identifier = @"recorridoLibre";
            } else if (indexPath.section == 1) {
                identifier = @"cuentaUsuario";
            } else if (indexPath.section == 2) {
                identifier = @"kmRecorridos";
            } else if (indexPath.section == 3) {
                identifier = @"rankingUsuarios";
            } else if (indexPath.section == 4) {
                identifier = @"contactoBicimad";
            }
            break;

        case 1:
            if (indexPath.section == 0) {
                identifier = @"tiempoMadrid";
            } else if (indexPath.section == 1) {
                identifier = @"addSaldo";
            } else {
                identifier =@"caloriasConsumidas";
            }
            break;
            
        case 2:
            if (indexPath.section == 0) {
                identifier = @"historialRutas";
            } else {
                identifier = @"huellaCO2";
            }
            break;
            
        case 3:
            identifier = @"tiempoMadrid";
            break;
    }
    
    return identifier;
}

//- (NSString *)segueIdentifierForIndexPathInRightMenu:(NSIndexPath *)indexPath
//{
//    NSString *identifier = @"";
//    switch (indexPath.row) {
//        case 0:
//            identifier = @"clubSocio";
//            break;
//    }
//    
//    return identifier;
//}

- (CGFloat)leftMenuWidth
{
    if ( IDIOM == IPAD ) {
        return sizeMenuIPAD;
    } else {
        return sizeMenuIPHONE;
    }
}

//- (CGFloat)rightMenuWidth
//{
//    return 250;
//}

- (void)configureLeftMenuButton:(UIButton *)button
{
    CGRect frame = button.frame;
    frame = CGRectMake(0, 0, 25, 13);
    button.frame = frame;
    button.backgroundColor = [UIColor clearColor];
    [button setImage:[UIImage imageNamed:@"simpleMenuButton"] forState:UIControlStateNormal];
}

//- (void)configureRightMenuButton:(UIButton *)button
//{
//    CGRect frame = button.frame;
//    frame = CGRectMake(0, 0, 25, 13);
//    button.frame = frame;
//    button.backgroundColor = [UIColor clearColor];
//    [button setImage:[UIImage imageNamed:@"simpleMenuButton"] forState:UIControlStateNormal];
//}

- (void) configureSlideLayer:(CALayer *)layer
{
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowOpacity = 1;
    layer.shadowOffset = CGSizeMake(0, 0);
    layer.shadowRadius = 5;
    layer.masksToBounds = NO;
    layer.shadowPath =[UIBezierPath bezierPathWithRect:self.view.layer.bounds].CGPath;
}

- (AMPrimaryMenu)primaryMenu
{
    return AMPrimaryMenuLeft;
}


// Enabling Deepnes on left menu
- (BOOL)deepnessForLeftMenu
{
    return NO;
}

// Enabling Deepnes on left menu
//- (BOOL)deepnessForRightMenu
//{
//    return YES;
//}

@end
