//
//  MapaViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 03/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

#import "TCGooglePlaces.h"
#import "TCGoogleDirections.h"
#import "BMGoogleAPIKeys.h"
#import "ComunObject.h"

@interface MapaViewController : UIViewController

@property (weak, nonatomic) IBOutlet GMSMapView *mapView_;
@property (nonatomic, strong) IBOutlet GMSMarker *mv;
@property (nonatomic, strong) NSString *city;
@property (weak, nonatomic) IBOutlet UISegmentedControl *vistaMapaControl;
@property NSUInteger pageIndex;

@property NSDictionary *coordenadas;

//- (IBAction)cambiarVistaMapa:(id)sender;
- (IBAction)setMyCoordenadas: (NSDictionary *) coordenadas;

@end
