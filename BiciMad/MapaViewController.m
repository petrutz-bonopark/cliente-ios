//
//  MapaViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 03/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "MapaViewController.h"

@interface MapaViewController ()

@end

// Botón en forma de candado abierto o cerrado, para controlar cuando el mapa está bloqueado para la interacción con el usuario.
UIButton *lockButton;

@implementation MapaViewController

@synthesize mv;
@synthesize city;
@synthesize mapView_;

- (void) setMyCoordenadas:(NSDictionary *)coordenadas
{
    if (coordenadas) {
        _coordenadas = coordenadas;
    }
}

- (id)init{
    self = [super init];
    if (self) {
        [GMSServices provideAPIKey:bMCGoogleMapsAPIKey];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mapView_.settings.myLocationButton = YES;
    mapView_.myLocationEnabled = YES;
    
    
    [self getDirectionsFromRoute];
    [self addLockButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getDirectionsFromRoute
{
    
    [ComunObject drawRouteWaypoints:mapView_ coordenadas:_coordenadas];

}

- (void)addLockButton
{
    
    lockButton = [[UIButton alloc] initWithFrame: CGRectMake(5, 60, 40.0f, 42.0f)];
    UIImage *backImage = [UIImage imageNamed:@"candado_cerrado"];
    [lockButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [lockButton addTarget:self action:@selector(unlockMap) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:lockButton];
}

- (void) lockMap
{
    [mapView_ setUserInteractionEnabled:FALSE];
    UIImage *backImage = [UIImage imageNamed:@"candado_cerrado"];
    [lockButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [lockButton addTarget:self action:@selector(unlockMap) forControlEvents:UIControlEventTouchUpInside];
}

- (void) unlockMap
{
    [mapView_ setUserInteractionEnabled:TRUE];
    UIImage *backImage = [UIImage imageNamed:@"candado_abierto"];
    [lockButton setBackgroundImage:backImage  forState:UIControlStateNormal];
    [lockButton addTarget:self action:@selector(lockMap) forControlEvents:UIControlEventTouchUpInside];
}

//- (void)getRouteFromCoordinates:(CLLocationCoordinate2D )iniLocation toLocation:(CLLocationCoordinate2D )lastLocation
//{
//    // Configure the parameters to be send to TCDirectionsService.
//    TCDirectionsParameters *parameters = [[TCDirectionsParameters alloc] init];
//    parameters.origin = iniLocation;
//    parameters.destination = lastLocation;
//    
//    [[TCDirectionsService sharedService] routeWithParameters:parameters completion:^(NSArray *routes, NSError *error) {
//        if (routes) {
//            // There should only be one route since we did not ask for alternative routes.
//            TCDirectionsRoute *route = routes[0];
//            
//            // Move camera viewport to fit the viewport bounding box of this route.
//            [mapView_ animateWithCameraUpdate:
//             [GMSCameraUpdate fitBounds:route.bounds]];
//            
//            [ComunObject drawRoute:route onMap:mapView_];
//            
//            [mapView_ setUserInteractionEnabled:FALSE];
//
//        } else {
//            NSLog(@"[Google Directions API] - Error: %@", [error localizedDescription]);
//        }
//    }];
//}

@end
