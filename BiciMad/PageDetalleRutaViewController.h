//
//  PageDetalleRutaViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 10/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetalleRutaViewController.h"

@interface PageDetalleRutaViewController : UIViewController <UIPageViewControllerDataSource>

- (IBAction)startWalkthrough:(id)sender;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray  *pageTitles;
@property (strong, nonatomic) NSArray  *pageImages;

@property (weak, nonatomic) IBOutlet UILabel *fechaRuta;
@property (weak, nonatomic) IBOutlet UILabel *duracionRuta;
@property (weak, nonatomic) IBOutlet UILabel *distanciaRuta;
@property (weak, nonatomic) IBOutlet UILabel *velocidadMediaRuta;
@property (weak, nonatomic) IBOutlet UILabel *parcialMedioRuta;
@property (weak, nonatomic) IBOutlet UILabel *caloriasRuta;
@property (weak, nonatomic) IBOutlet UILabel *huellaCO2Ruta;
@property (weak, nonatomic) IBOutlet UILabel *velocidadMaxRuta;
@property (weak, nonatomic) IBOutlet UILabel *elevacionRuta;

@property (strong, nonatomic) RutaObject *rutaObject;

- (void) setMyRuta: (RutaObject *) ruta;

@end
