//
//  PageViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 10/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"
#import "DetalleRutaViewController.h"
#import "MapaViewController.h"
#import "CPDScatterPlotViewController.h"

@interface PageViewController : UIViewController <UIPageViewControllerDataSource>

- (IBAction)startWalkthrough:(id)sender;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;

@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@property (strong, nonatomic) IBOutlet UIView *viewControl;

@property (strong, nonatomic) RutaObject *rutaObject;
- (void) setMyRuta: (RutaObject *) ruta;

@property (strong, nonatomic) IBOutlet UIPageControl *myPageControl;

@end
