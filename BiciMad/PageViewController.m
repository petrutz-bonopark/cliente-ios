//
//  PageViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 10/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "PageViewController.h"

@interface PageViewController ()

@end

@implementation PageViewController

@synthesize rutaObject;

- (void) setMyRuta:(RutaObject *)ruta {
    
    if (ruta) {
        rutaObject = ruta;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Create the data model
    _pageTitles = @[@"Datos de la ruta", @"Ruta en el mapa", @"Tabla de Altimetría"];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    UIViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
//    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [[self.pageViewController view] setFrame:[[self view] bounds]];
    self.pageViewController.view.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    self.pageViewController.view.backgroundColor = [UIColor clearColor];
    
    [self addChildViewController:_pageViewController];
    [self.viewContainer addSubview:_pageViewController.view];

    [self.pageViewController didMoveToParentViewController:self];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startWalkthrough:(id)sender {
    UIViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    DetalleRutaViewController *detalleRutaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"detalleRutaViewController"];
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    MapaViewController *mapaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MapaViewController"];
    CPDScatterPlotViewController *cpdPlot = [self.storyboard instantiateViewControllerWithIdentifier:@"scatterPlot"];
    switch (index) {
        case 0:
            detalleRutaViewController.pageIndex = index;
            [detalleRutaViewController setMyRuta:rutaObject];
            return detalleRutaViewController;
            break;
        case 1:
            mapaViewController.pageIndex = index;
            [mapaViewController setMyCoordenadas:rutaObject.coordenadasRuta];
            return mapaViewController;
            break;
        case 2:
            cpdPlot.pageIndex = index;
            [cpdPlot setMyCoordenadas:rutaObject.coordenadasRuta];
            return cpdPlot;
            break;
//        case 3:
//            pageContentViewController.imageFile = self.pageImages[index];
//            pageContentViewController.titleText = self.pageTitles[index];
//            pageContentViewController.pageIndex = index;
//            return pageContentViewController;
//            break;
        default:
            break;
    }

    
    return pageContentViewController;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;

    self.myPageControl.currentPage = index;
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    self.myPageControl.currentPage = index;
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

@end
