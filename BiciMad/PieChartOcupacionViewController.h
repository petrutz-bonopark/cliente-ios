//
//  PieChartOcupacionViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 06/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"

@class PieChart;

@interface PieChartOcupacionViewController : UIViewController<XYPieChartDelegate, XYPieChartDataSource>


@property (strong, nonatomic) IBOutlet XYPieChart *pieChartLeft;

@property (strong, nonatomic) IBOutlet UILabel *selectedSliceLabel;
@property (strong, nonatomic) IBOutlet UITextField *numOfSlices;
@property (strong, nonatomic) IBOutlet UISegmentedControl *indexOfSlices;
@property(nonatomic, strong) NSMutableArray *slices;
@property(nonatomic, strong) NSArray        *sliceColors;
@property (weak, nonatomic) IBOutlet UILabel *totalBases;
@property (weak, nonatomic) IBOutlet UILabel *basesLibres;

- (void) setValuesSlice: (NSMutableArray *) array total:(NSString *) total libres: (NSString *) libres;
@end
