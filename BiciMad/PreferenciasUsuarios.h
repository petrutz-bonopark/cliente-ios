//
//  PreferenciasUsuarios.h
//  BiciMad
//
//  Created by Miguel Asuar on 04/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * const PREFERENCIA_DNI = @"dni_user";
static NSString * const PREFERENCIA_ID_AUTH = @"id_auth";
static NSString * const PREFERENCIA_NOMBRE = @"name_user";
static NSString * const PREFERENCIA_APELLIDO = @"apellido";
static NSString * const PREFERENCIA_APELLIDO2 = @"apellido2";
static NSString * const PREFERENCIA_DNI_TUTOR = @"dni_user_tutor";
static NSString * const PREFERENCIA_NOMBRE_TUTOR = @"name_user_tutor";
static NSString * const PREFERENCIA_APELLIDO_TUTOR = @"apellido_tutor";
static NSString * const PREFERENCIA_APELLIDO2_TUTOR = @"apellido2_tutor";
static NSString * const PREFERENCIA_USUARIO_CON_TUTOR = @"usuario_con_tutor";
static NSString * const PREFERENCIA_EMAIL = @"email_user";
static NSString * const PREFERENCIA_FECHA_NAC = @"fecha_nacimiento";
static NSString * const PREFERENCIA_TELEFONO = @"telefono";
static NSString * const PREFERENCIA_DIRECCION = @"direccion";
static NSString * const PREFERENCIA_MUNICIPIO = @"municipio";
static NSString * const PREFERENCIA_PROVINCIA = @"provincia";
static NSString * const PREFERENCIA_COD_POSTAL = @"codigo_postal";

static NSString * const PREFERENCIA_RFID = @"idrfid";

static NSString * const PREFERENCIA_TIPO = @"type_user";
static NSString * const PREFERENCIA_ESCALA_UNIDADES = @"escala_unidades";
static NSString * const PREFERENCIA_UNIDADES_TEMPERATURA = @"unidades_temperatura";
static NSString * const PREFERENCIA_SAVE_SESION = @"save_sesion";
static NSString * const PREFERENCIA_LEYENDA_MAPA = @"show_leyenda_mapa";
static NSString * const PREFERENCIA_CONTADOR_ACCESOS = @"contador_accesos";

static NSString * const PREFERENCIA_DEV_TOKEN = @"dev_token";

@interface PreferenciasUsuarios : NSObject

+ (void) saveObjectInPreferences: (NSString *) key value: (NSString *) value;

+ (void) saveIntegerInPreferences: (NSString *) key value: (NSInteger) value;

+ (void) saveBoolInPreferences: (NSString *) key value: (BOOL) value;

+ (NSString *) getStringInPreferences: (NSString *) key;

+ (int) getIntInPreferences: (NSString *) key;

+ (BOOL) getBooleanInPreferences: (NSString *) key;

+ (void) removeObjectInPreferences: (NSString *) key;

+ (void)resetDefaults;

+ (void) showAllPreferences;

@end
