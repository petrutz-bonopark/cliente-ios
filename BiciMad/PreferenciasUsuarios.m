//
//  PreferenciasUsuarios.m
//  BiciMad
//
//  Created by Miguel Asuar on 04/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "PreferenciasUsuarios.h"

@implementation PreferenciasUsuarios


+ (void) saveObjectInPreferences: (NSString *) key value: (NSString *) value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:key];
    [defaults synchronize];
}

+ (void) saveIntegerInPreferences: (NSString *) key value: (NSInteger) value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:value forKey:key];
    [defaults synchronize];
}

+ (void) saveBoolInPreferences: (NSString *) key value: (BOOL) value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:value forKey:key];
    [defaults synchronize];
}

+ (NSString *) getStringInPreferences: (NSString *) key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults stringForKey:key];
    [defaults synchronize];

}

+ (int) getIntInPreferences: (NSString *) key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return (int) [defaults integerForKey:key];
    [defaults synchronize];
    
}

+ (BOOL) getBooleanInPreferences: (NSString *) key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults boolForKey:key];
    [defaults synchronize];
    
}

+ (void) removeObjectInPreferences: (NSString *) key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:key];
    [defaults synchronize];
}

+ (void)resetDefaults {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defaults dictionaryRepresentation];
    for (id key in dict) {
        [defaults removeObjectForKey:key];
    }
    [defaults synchronize];
}

+ (void) showAllPreferences
{
    NSLog(@"DNI: %@\n Nombre: %@\n Apellidos: %@ %@\n Fecha nacimiento: %@\n Email %@\n Accesos: %@\n Dev token: %@",
          [self getStringInPreferences:PREFERENCIA_DNI],
          [self getStringInPreferences:PREFERENCIA_NOMBRE],
          [self getStringInPreferences:PREFERENCIA_APELLIDO],
          [self getStringInPreferences:PREFERENCIA_APELLIDO2],
          [self getStringInPreferences:PREFERENCIA_FECHA_NAC],
          [self getStringInPreferences:PREFERENCIA_EMAIL],
          [self getStringInPreferences:PREFERENCIA_CONTADOR_ACCESOS],
          [self getStringInPreferences:PREFERENCIA_DEV_TOKEN]);

}

@end
