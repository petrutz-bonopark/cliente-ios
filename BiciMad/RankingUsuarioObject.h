//
//  RankingUsuarioObject.h
//  BiciMad
//
//  Created by Miguel Asuar on 11/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RankingUsuarioObject : NSObject

@property (copy, nonatomic) NSString *posicion;
@property (copy, nonatomic) NSString *usuario;
@property (copy, nonatomic) NSString *usos;
@property (copy, nonatomic) NSString *distancia;
@property (copy, nonatomic) NSString *duracion;
@property (copy, nonatomic) NSString *huellaCO2;

- (id)initWithText:(NSString *) posicion usuario: (NSString *) usuario usos:(NSString *) usos distancia:(NSString *) distancia duracion:(NSString *) duracion huellaCO2:(NSString *) huellaCO2;
@end
