//
//  RankingUsuarioObject.m
//  BiciMad
//
//  Created by Miguel Asuar on 11/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "RankingUsuarioObject.h"

@implementation RankingUsuarioObject

- (id)initWithText:(NSString *) posicion usuario: (NSString *) usuario usos:(NSString *) usos distancia:(NSString *) distancia duracion:(NSString *) duracion huellaCO2: (NSString *) huellaCO2
{
    self = [super init];
    if (self) {
        _posicion = [posicion copy];
        _usuario = [usuario copy];
        _usos = [usos copy];
        _distancia = [distancia copy];
        _duracion = [duracion copy];
        _huellaCO2 = [huellaCO2 copy];
    }
    
    return self;
}
@end
