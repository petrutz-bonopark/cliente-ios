//
//  RankingUsuariosCell.h
//  BiciMad
//
//  Created by Miguel Asuar on 11/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RankingUsuariosCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelPosicion;
@property (weak, nonatomic) IBOutlet UILabel *labelUsuario;
@property (weak, nonatomic) IBOutlet UILabel *labelUsos;
@property (weak, nonatomic) IBOutlet UILabel *labelKM;
@property (weak, nonatomic) IBOutlet UILabel *labelHuellaCO2;
@property (strong, nonatomic) IBOutlet UILabel *labelMillas;

@end
