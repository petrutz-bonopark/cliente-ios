//
//  RankingUsuariosCell.m
//  BiciMad
//
//  Created by Miguel Asuar on 11/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "RankingUsuariosCell.h"

@implementation RankingUsuariosCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
