//
//  RankingUsuariosViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 11/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComunObject.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <AFNetworking/AFNetworking.h>
#import "RankingUsuariosCell.h"
#import "RankingUsuarioObject.h"

@interface RankingUsuariosViewController : UITableViewController

@property(nonatomic, strong) NSMutableArray *rankingUsuarios;

@end
