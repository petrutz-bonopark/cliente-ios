//
//  RankingUsuariosViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 11/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "RankingUsuariosViewController.h"

@interface RankingUsuariosViewController ()

@end

@implementation RankingUsuariosViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self getRankingUsuarios];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getRankingUsuarios
{
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    progressHUD.dimBackground = YES;
    progressHUD.labelText = @"Obteniendo estado de las estaciones";
    
    NSDictionary *myJson=@{TAG_DNI: [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DNI],
                           TAG_ID_AUTH: [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_ID_AUTH]};
    
    NSURL *url = [NSURL URLWithString:BaseURLString];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFJSONParameterEncoding;
    NSDictionary *params = myJson ;
    
    [httpClient postPath:[NSString stringWithFormat:@"%@?format=json", url_get_ranking_usuarios] parameters:params
                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                     // Print the response body in text
                     NSLog(@"Write to DB: Success");
                     NSError *error ;
                     NSDictionary* JSON = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                     NSLog(@"Return string: %@", JSON);
                     
                     self.rankingUsuarios = [[NSMutableArray alloc] init];
                     int posiciones = 0;
                     
                     NSDictionary *rutaJSON = [JSON objectForKey:TAG_RANKINGS_USUARIO];
                     for (NSDictionary *rankingsUsuarios in rutaJSON) {
                         posiciones++;
                         
                         NSString *usuario = [rankingsUsuarios objectForKey:TAG_DNI];
                         NSString *usos = [rankingsUsuarios objectForKey:TAG_USOS];
                         NSString *distancia = [rankingsUsuarios objectForKey:TAG_DISTANCIA_TOTAL];
                         NSString *duracion = [rankingsUsuarios objectForKey:TAG_DURACION_TOTAL];
                         NSString *huellaCO2 = [rankingsUsuarios objectForKey:TAG_HUELLA_CO2_TOTAL];
                         
                         RankingUsuarioObject * ranking = [[RankingUsuarioObject alloc] initWithText:[NSString stringWithFormat:@"%i", posiciones] usuario:usuario usos:usos distancia:distancia duracion:duracion huellaCO2:huellaCO2];
                         [self.rankingUsuarios addObject:ranking];
                     }
                     
                     [self.tableView reloadData];
                     
                     [progressHUD hide:YES];
                     
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Write to DB: Fail");
                     [progressHUD hide:YES];
                     
                     [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"LABEL_ERROR_CONNECTION"];
                 }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.rankingUsuarios count];
}

- (RankingUsuariosCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RankingCell";
    RankingUsuariosCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[RankingUsuariosCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    RankingUsuarioObject *ranking = [self.rankingUsuarios objectAtIndex:indexPath.row];
    //NSLog([NSString stringWithFormat:@"Usuario ranking: %@ Usuario sesion: %@", ranking.usuario, [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_EMAIL]]);
    if ([[PreferenciasUsuarios getStringInPreferences:PREFERENCIA_EMAIL] isEqualToString:ranking.usuario])
    {
        cell.labelUsuario.textColor = [UIColor blueColor];
    }
    cell.labelPosicion.text = ranking.posicion;
    cell.labelUsuario.text = ranking.usuario;
    cell.labelUsos.text = ranking.usos;
    cell.labelKM.text = [NSString stringWithFormat:@"%.02f", [ranking.distancia floatValue]];
    cell.labelHuellaCO2.text = [NSString stringWithFormat:@"%.02f", [ranking.huellaCO2 floatValue]];
    cell.labelMillas.text = [self getMillasUsuario:ranking.usos km:ranking.distancia];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IDIOM == IPAD) {
        return 100;
    }
    
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (NSString*) getMillasUsuario:(NSString*) usos km:(NSString*) km
{

    int indiceMilla = 10;
    
    return [NSString stringWithFormat:@"%i", indiceMilla * [km intValue] + [usos intValue]];

}

@end
