//
//  RecargaSaldoViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 31/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebFormTPVViewController.h"

@interface RecargaSaldoViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *textImporte;
@property (strong, nonatomic) IBOutlet UIStepper *myStepper;

@property NSInteger importe;

- (IBAction)cambiarImporte:(UIStepper*)sender;
- (IBAction)cerrarRecarga:(id)sender;
@end
