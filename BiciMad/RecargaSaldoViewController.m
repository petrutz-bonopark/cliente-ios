//
//  RecargaSaldoViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 31/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "RecargaSaldoViewController.h"

@interface RecargaSaldoViewController ()

@end

@implementation RecargaSaldoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // self.myStepper.maximumValue = 10;
    self.myStepper.minimumValue = 10;
    self.myStepper.stepValue=1.0;
    
    self.importe = 10;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cambiarImporte:(UIStepper*)sender {
    double value = [sender value];
    self.importe = value;
    [self.textImporte setText:[NSString stringWithFormat:@"%d €", (int)value]];
}

- (IBAction)cerrarRecarga:(id)sender {
    
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"recargarSaldoTPV"]) {
        WebFormTPVViewController *controller = segue.destinationViewController;
        controller.importePago = self.importe;
        controller.tipoOperacion = TIPO_PAGO_RECARGA;
    }
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.textImporte) {
        [textField resignFirstResponder];
    }
}
@end
