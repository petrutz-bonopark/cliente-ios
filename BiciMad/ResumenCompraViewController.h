//
//  ResumenCompraViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 10/04/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TipoDePagoViewController.h"
#import "WebFormTPVViewController.h"

@interface ResumenCompraViewController : UITableViewController<TipoDePagoDelegate>

@property (assign, nonatomic) NSInteger importePago;
@property (assign, nonatomic) NSString * nombreUsuario;
@property (assign, nonatomic) NSString * apellidoUsuario;
@property (assign, nonatomic) NSString * apellido2Usuario;
@property (assign, nonatomic) NSString * emailUsuario;

@property (strong, nonatomic) IBOutlet UILabel *labelNombreUsuario;
@property (strong, nonatomic) IBOutlet UILabel *labelApellidosUsuario;
@property (strong, nonatomic) IBOutlet UILabel *labelEmailUsuario;
@property (strong, nonatomic) IBOutlet UILabel *labelImporteUsuario;

@property (strong, nonatomic) IBOutlet UITableViewCell *metodoPagoCell;

- (IBAction)validarCompra:(id)sender;
@end
