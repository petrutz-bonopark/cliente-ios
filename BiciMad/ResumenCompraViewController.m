//
//  ResumenCompraViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 10/04/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "ResumenCompraViewController.h"

@interface ResumenCompraViewController ()

@end

@implementation ResumenCompraViewController


@synthesize nombreUsuario;
@synthesize apellidoUsuario;
@synthesize apellido2Usuario;
@synthesize emailUsuario;
@synthesize importePago;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.labelNombreUsuario.text = self.nombreUsuario;
    self.labelApellidosUsuario.text = [NSString stringWithFormat:@"%@ %@", self.apellidoUsuario, self.apellido2Usuario];
    self.labelEmailUsuario.text = self.emailUsuario;
    self.labelImporteUsuario.text = [NSString stringWithFormat:@"%ld €", (long)self.importePago];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)validarCompra:(id)sender {
    
    [self performSegueWithIdentifier:@"ValidarCompra" sender:self];
}

- (void)updateMetodoPago:(NSInteger) metodoPago
{

    switch (metodoPago) {
        case 1:
            self.metodoPagoCell.textLabel.text = @"Tarjeta de crédito";
            self.metodoPagoCell.tag = metodoPago;
            self.metodoPagoCell.imageView.image = [UIImage imageNamed:@"credit_cards.png"];
            break;

        case 2:
            self.metodoPagoCell.textLabel.text = @"Cuenta de PayPal";
            self.metodoPagoCell.tag = metodoPago;
            self.metodoPagoCell.imageView.image = [UIImage imageNamed:@"paypal_01.png"];
            break;
            
        default:
            break;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"MetodoPago"]) {
        // Get the destination controller
        TipoDePagoViewController *controller = segue.destinationViewController;
    
        // Store the AppCategoryEntity on the destination controller
        controller.tipoDePago = self.metodoPagoCell.tag;
    
        // Store a reference to this view controller in the
        // delegate property of the destination view controller
        controller.delegate = self;
    } else if([segue.identifier isEqualToString:@"ValidarCompra"]) {
        WebFormTPVViewController *controller = segue.destinationViewController;
        controller.importePago = self.importePago;
        controller.tipoOperacion = TIPO_PAGO_ALTA;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView
                       cellForRowAtIndexPath:indexPath];
    
    return cell;
}

@end
