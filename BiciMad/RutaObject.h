//
//  RutaObject.h
//  BiciMad
//
//  Created by Miguel Asuar on 10/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RutaObject : NSObject

@property (copy, nonatomic) NSString *fechaRuta;
@property (copy, nonatomic) NSString *duracionRuta;
@property (copy, nonatomic) NSString *distanciaRuta;
@property (copy, nonatomic) NSString *velocidadMediaRuta;
@property (copy, nonatomic) NSString *parcialMedioRuta;
@property (copy, nonatomic) NSString *caloriasRuta;
@property (copy, nonatomic) NSString *huellaCO2Ruta;
@property (copy, nonatomic) NSString *velocidadMaxRuta;
@property (copy, nonatomic) NSString *elevacionRuta;
@property (copy, nonatomic) NSDictionary *coordenadasRuta;

- (id)initWithText:(NSString *)fecha duracion:(NSString *)duracion distancia:(NSString *)distancia velocidadMedia:(NSString *)velocidadMedia parcial:(NSString *)parcial calorias:(NSString *)calorias huellaCO2:(NSString *)huellaCO2 velocidadMax:(NSString *)velocidadMax elevacion:(NSString *)elevacion coordenadas:(NSDictionary *) coordenadas;

@end
