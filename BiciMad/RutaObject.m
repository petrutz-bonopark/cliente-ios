//
//  RutaObject.m
//  BiciMad
//
//  Created by Miguel Asuar on 10/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "RutaObject.h"

@implementation RutaObject

- (id)initWithText:(NSString *)fecha duracion:(NSString *)duracion distancia:(NSString *)distancia velocidadMedia:(NSString *)velocidadMedia parcial:(NSString *)parcial calorias:(NSString *)calorias huellaCO2:(NSString *)huellaCO2 velocidadMax:(NSString *)velocidadMax elevacion:(NSString *)elevacion coordenadas:(NSDictionary *)coordenadas
{
    self = [super init];
    if (self) {
        _fechaRuta = [fecha copy];
        _duracionRuta = [duracion copy];
        _distanciaRuta = [distancia copy];
        _velocidadMediaRuta = [velocidadMedia copy];
        _parcialMedioRuta = [parcial copy];
        _caloriasRuta = [calorias copy];
        _huellaCO2Ruta = [huellaCO2 copy];
        _velocidadMaxRuta = [velocidadMax copy];
        _elevacionRuta = [elevacion copy];
        _coordenadasRuta = [coordenadas copy];
    }
    return self;
}

@end
