//
//  SplashViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 28/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PreferenciasUsuarios.h"
#import "TutorialViewController.h"

@interface SplashViewController : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;

@property (weak, nonatomic) IBOutlet UIImageView *biciImage;
@property (weak, nonatomic) IBOutlet UIImageView *bicimadImage;
@property (strong, nonatomic) IBOutlet UIPageControl *myPageControl;

@property (weak, nonatomic) IBOutlet UIButton *buttonAlta;
@property (weak, nonatomic) IBOutlet UIButton *buttonEntrar;
@property (weak, nonatomic) IBOutlet UIView *tutorialContainer;
@end
