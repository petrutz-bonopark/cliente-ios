//
//  SplashViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 28/02/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "SplashViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

@synthesize biciImage;
@synthesize bicimadImage;
@synthesize buttonAlta;
@synthesize buttonEntrar;

NSInteger numScreenTutorial = 5;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    CALayer *btnEntrarLayer = [buttonEntrar layer];
    [btnEntrarLayer setMasksToBounds:YES];
    [btnEntrarLayer setCornerRadius:3.0f];
    buttonEntrar.alpha = 0;
    
    CALayer *btnAltaLayer = [buttonAlta layer];
    [btnAltaLayer setMasksToBounds:YES];
    [btnAltaLayer setCornerRadius:3.0f];
    buttonAlta.alpha = 0;
   
    float posInicialX = -self.biciImage.bounds.size.width;
    float posFinalX = self.biciImage.bounds.size.width / 2;
    float posInicialY = self.biciImage.center.y;
    
    self.biciImage.center = CGPointMake(posInicialX, posInicialY);
    
    CGPoint finalPosition = CGPointMake(posFinalX, posInicialY);
    
    [UIView animateWithDuration:3 delay:0 options:UIViewAnimationOptionCurveLinear
					 animations:^ {
						 self.biciImage.center = finalPosition;
					 }completion:^(BOOL finished) {
                         if (finished) {
                             NSString * dni = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DNI];
                             NSString * openSesion = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_SAVE_SESION];
                             if (dni != nil) {
                                 NSInteger contadorAccesos = [PreferenciasUsuarios getIntInPreferences:PREFERENCIA_CONTADOR_ACCESOS];
                                 contadorAccesos++;
                                 [PreferenciasUsuarios saveIntegerInPreferences:PREFERENCIA_CONTADOR_ACCESOS value:contadorAccesos];
                                 // Si el usuario ha accedido 10 veces o más dejamos de mostrar la leyenda en el mapa
                                 if (contadorAccesos >= 3) {
                                     [PreferenciasUsuarios saveIntegerInPreferences:PREFERENCIA_LEYENDA_MAPA value:2];
                                 }
                                 if ([openSesion isEqualToString:@"YES"]) {
                                     [self performSegueWithIdentifier:@"SesionAbierta" sender:self];
                                 } else {
                                     [self fadein:buttonAlta];
                                     [self fadein:buttonEntrar];
                                 }
                             } else {
                                 [self fadein:buttonAlta];
                                 [self fadein:buttonEntrar];
                             }
                         }
                     }];
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    CGRectMake(0, self.view.bounds.size.height,
               self.view.bounds.size.width,
               self.view.bounds.size.height);
    [[self.pageController view] setFrame:[[self view] bounds]];
    self.pageController.view.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    
    TutorialViewController *initialViewController = [self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [self.tutorialContainer addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    
    // TRAZAS
    [PreferenciasUsuarios showAllPreferences];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) fadein: (UIButton *) button
{
    button.alpha = 0;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    
    //don't forget to add delegate.....
    [UIView setAnimationDelegate:self];
    
    [UIView setAnimationDuration:2];
    button.alpha = 1;
    
    //also call this before commit animations......
    [UIView commitAnimations];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(TutorialViewController *)viewController pageIndex];
    
    if (index == 0) {
        self.myPageControl.currentPage = 0;
        return nil;
    } else {
        self.myPageControl.currentPage = index;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(TutorialViewController *)viewController pageIndex];
    
    
    index++;
    
    if (index == numScreenTutorial) {
        self.myPageControl.currentPage = numScreenTutorial;
        return nil;
    } else {
        self.myPageControl.currentPage = index - 1;
    }
    
    return [self viewControllerAtIndex:index];
    
}

- (TutorialViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    TutorialViewController *childViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialStoryboardID"];
    childViewController.pageIndex = index;
    
    return childViewController;
    
}

//- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
//    // The number of items reflected in the page indicator.
//    return 4;
//}
//
//- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
//    // The selected item reflected in the page indicator.
//    return 0;
//}

@end
