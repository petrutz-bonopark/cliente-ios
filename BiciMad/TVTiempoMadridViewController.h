//
//  TVTiempoMadridViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 05/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD.h>
#import <AFNetworking/AFNetworking.h>
#import "ComunObject.h"

@interface TVTiempoMadridViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageTiempo;

@property (weak, nonatomic) IBOutlet UILabel *tempIni;
@property (weak, nonatomic) IBOutlet UILabel *tempMaxMinIni;

@property (weak, nonatomic) IBOutlet UILabel *temperatura;
@property (weak, nonatomic) IBOutlet UILabel *presion;
@property (weak, nonatomic) IBOutlet UILabel *humedad;
@property (weak, nonatomic) IBOutlet UILabel *viento;

@end
