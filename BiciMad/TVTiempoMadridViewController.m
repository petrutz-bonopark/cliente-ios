//
//  TVTiempoMadridViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 05/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "TVTiempoMadridViewController.h"

@interface TVTiempoMadridViewController ()

@end

@implementation TVTiempoMadridViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

//    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondo_noche.jpg"]];
//    [tempImageView setFrame:self.tableView.frame];
//    
//    self.tableView.backgroundView = tempImageView;

    [self getWeatherMadrid];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)getWeatherMadrid
{
    NSString * uniTemp = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_UNIDADES_TEMPERATURA];
    NSString * uniTempRequest;
    if ([uniTemp isEqualToString:@"C"]) {
        uniTempRequest = @"&units=metric";
    } else {
        uniTempRequest = @"";
    }
    
    NSString  *estacionesUrl = [NSString  stringWithFormat:@"%@%@", @"http://api.openweathermap.org/data/2.5/weather?q=Madrid,Spain", uniTempRequest];
    NSURL  *url = [NSURL  URLWithString:estacionesUrl];
    NSURLRequest  *request = [NSURLRequest  requestWithURL:url];
    
    // Mostrar progressHUD mientras se registra la ruta en BD
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    progressHUD.dimBackground = YES;
    progressHUD.labelText = @"Obteniendo tiempo en Madrid...";
    
    // Todos los toques de pantalla deshabilitan el progressHUD
    progressHUD.userInteractionEnabled = YES;
    
    // 2
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
     // 3
                                                    success:^(NSURLRequest  *request, NSHTTPURLResponse  *response, id JSON) {
                                                        
                                                        NSDictionary *weatherJSON = [JSON objectForKey:@"weather"];
                                                        for (NSDictionary *ruta in weatherJSON) {
                                                            NSDictionary *main = [JSON objectForKey:@"main"];
                                                            _temperatura.text = [NSString stringWithFormat:@"%0.0f º%@", [[main valueForKey:@"temp"] floatValue], uniTemp];
                                                            _tempIni.text = [NSString stringWithFormat:@"%0.0f º%@", [[main valueForKey:@"temp"] floatValue], uniTemp];
                                                            _tempMaxMinIni.text = [NSString stringWithFormat:@"%0.0fº/%0.0fº", [[main valueForKey:@"temp_min"] floatValue], [[main valueForKey:@"temp_max"] floatValue]];
                                                            
                                                            _presion.text = [NSString stringWithFormat:@"%@ hPa", [main valueForKey:@"pressure"]];
                                                            _humedad.text = [NSString stringWithFormat:@"%@ %@", [main valueForKey:@"humidity"], @"%"];
                                                            
                                                            NSDictionary *wind = [JSON objectForKey:@"wind"];
                                                            _viento.text = [NSString stringWithFormat:@"%@ %@", [wind valueForKey:@"speed"], @"km/h"];
                                                            
                                                            NSString * icon = [ruta objectForKey:@"icon"];
                                                            self.imageTiempo.image = [UIImage imageNamed:icon];
                                                        }
                                                        
                                                        
                                                        [progressHUD hide:YES];
                                                        
                                                    }
     // 4
                                                    failure:^(NSURLRequest  *request, NSHTTPURLResponse  *response, NSError  *error, id JSON) {
                                                        [ComunObject alertError:@"TITULO_ALERT_ERROR" idTexto:@"LABEL_ERROR_CONNECTION"];
                                                        
                                                        [progressHUD hide:YES];
                                                    }];
    
    // 5
    [operation start];
}

@end
