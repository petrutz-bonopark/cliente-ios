//
//  TableViewCuentaUsuarioViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 05/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD.h>
#import "PreferenciasUsuarios.h"
#import "ComunObject.h"
#import "DatosUsuarioViewController.h"

@interface TableViewCuentaUsuarioViewController : UITableViewController<CambioDatosDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nombreUsuario;
@property (weak, nonatomic) IBOutlet UILabel *saldoUsuario;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentTemperatura;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentEscala;

- (IBAction)changeMedidaTemperatura:(id)sender;
- (IBAction)changeEscalaUnidades:(id)sender;

- (IBAction)cerrarSesion:(id)sender;
@end
