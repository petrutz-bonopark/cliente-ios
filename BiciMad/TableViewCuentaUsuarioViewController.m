//
//  TableViewCuentaUsuarioViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 05/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "TableViewCuentaUsuarioViewController.h"

@interface TableViewCuentaUsuarioViewController ()

@end

@implementation TableViewCuentaUsuarioViewController

bool tieneTarjeta = false;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    _nombreUsuario.text = [NSString stringWithFormat:@"%@ %@",
        [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_NOMBRE],
        [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO]];
    
    NSString * unidades = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_ESCALA_UNIDADES];
    
    NSString * temperatura = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_UNIDADES_TEMPERATURA];
    
    if ([temperatura isEqualToString:@"C"])
    {
        _segmentTemperatura.selectedSegmentIndex = 0;
    } else {
        _segmentTemperatura.selectedSegmentIndex = 1;
    }
    
    if ([unidades isEqualToString:@"km"])
    {
        _segmentEscala.selectedSegmentIndex = 0;
    } else {
        _segmentEscala.selectedSegmentIndex = 1;
    }
    
    [self checkSaldoUsuario];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1 && !tieneTarjeta) {
        return 1;
    } else {
        return [super tableView:tableView numberOfRowsInSection:section];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView
                       cellForRowAtIndexPath:indexPath];
    
    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"cambioDatosUsuario"]) {
        // Get the destination controller
        DatosUsuarioViewController *controller = segue.destinationViewController;
        
        controller.delegate = self;
    }
}

- (void) checkSaldoUsuario
{
    NSString * idAuth = [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_ID_AUTH];
    if (idAuth != NULL) {
        
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    progressHUD.dimBackground = YES;
    progressHUD.labelText = @"Obteniendo datos de la cuenta...";
    
    NSDictionary *myJson=@{TAG_DNI: [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DNI],
                           TAG_ID_AUTH: [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_ID_AUTH]};
    
    NSURL *url = [NSURL URLWithString:BaseURLString];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFJSONParameterEncoding;
    NSDictionary *params = myJson ;
    
    [httpClient postPath:url_get_datos_usuario parameters:params
                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                     // Print the response body in text
                     NSLog(@"Write to DB: Success");
                     NSError *error ;
                     NSDictionary* jsonFromData = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                     NSLog(@"Return string: %@", jsonFromData);
                     
                     NSString *success = [jsonFromData objectForKey:TAG_SUCCESS];
                     
                     if ([success integerValue] == 1) {
                         
                         tieneTarjeta = true;
                         
                         NSDictionary * usuarioAlta = [jsonFromData objectForKey:TAG_USUARIO];
                         
                         for (NSDictionary * usuario in usuarioAlta) {
                             _saldoUsuario.text = [NSString stringWithFormat:@"%@ €", [usuario objectForKey:TAG_SALDO]];
                         }
                         
                        [self.tableView reloadData];
                     } else if ([success integerValue] == 2) {
                     
                         tieneTarjeta = false;
                         
                         [self.tableView reloadData];
                         
                         _saldoUsuario.text = NSLocalizedStringFromTable(@"MENSAJE_RECOGER_TARJETA", @"messages", nil);
                         _saldoUsuario.textColor = [UIColor redColor];
                         [ComunObject alertError:@"MENSAJE_RECOGER_TARJETA_TITULO" idTexto:@"MENSAJE_RECOGER_TARJETA_MENSAJE"];
                     }
                     [progressHUD hide:YES];
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Write to DB: Fail");
                     
                     tieneTarjeta = false;
                     
                     [progressHUD hide:YES];
                 }];
    }
}

- (IBAction)changeMedidaTemperatura:(id)sender {
    
    switch (((UISegmentedControl *) sender).selectedSegmentIndex) {
        case 0:
            [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_UNIDADES_TEMPERATURA value:@"C"];
        break;
            
        case 1:
            [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_UNIDADES_TEMPERATURA value:@"T"];
        break;
    }
}

- (IBAction)changeEscalaUnidades:(id)sender {

    switch (((UISegmentedControl *) sender).selectedSegmentIndex) {
        case 0:
            [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_ESCALA_UNIDADES value:@"km"];
        break;
            
        case 1:
            [PreferenciasUsuarios saveObjectInPreferences:PREFERENCIA_ESCALA_UNIDADES value:@"mi"];            
        break;
            
    }
}

- (IBAction)cerrarSesion:(id)sender {
    [ComunObject removePreferenciasUsuario];
}

- (void)updatedatosUsuario
{
    
    _nombreUsuario.text = [NSString stringWithFormat:@"%@ %@",
                           [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_NOMBRE],
                           [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO]];
}

@end
