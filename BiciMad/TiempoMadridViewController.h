//
//  TiempoMadridViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 05/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD.h>
#import <AFNetworking/AFNetworking.h>
#import "ComunObject.h"

@interface TiempoMadridViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *containerView;
@end
