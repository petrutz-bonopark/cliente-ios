//
//  TipoDePagoViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 10/04/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TipoDePagoDelegate <NSObject>

- (void)updateMetodoPago:(NSInteger) metodoPago;

@end

@interface TipoDePagoViewController : UITableViewController

@property (nonatomic, weak) id<TipoDePagoDelegate> delegate;

@property (assign, nonatomic) NSInteger tipoDePago;

@end
