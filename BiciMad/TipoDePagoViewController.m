//
//  TipoDePagoViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 10/04/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "TipoDePagoViewController.h"

@interface TipoDePagoViewController ()

@end

@implementation TipoDePagoViewController
{

    NSIndexPath *oldIndexPath;
}
@synthesize tipoDePago;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView
                       cellForRowAtIndexPath:indexPath];
    
    // Check/uncheck the current cell
	if (cell.tag == self.tipoDePago)
	{
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
		oldIndexPath = indexPath;
	}
	else {
		cell.accessoryType = UITableViewCellAccessoryNone;
	}
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Deselect the currently selected row
	[self.tableView deselectRowAtIndexPath:
     [self.tableView indexPathForSelectedRow] animated:NO];
    
	// Uncheck the previously selected cell, check the currently selected cell
	[tableView cellForRowAtIndexPath:oldIndexPath].accessoryType =
	UITableViewCellAccessoryNone;
    
	[tableView cellForRowAtIndexPath:indexPath].accessoryType =
	UITableViewCellAccessoryCheckmark;
    
	// Save the current indexPath for later
	oldIndexPath = indexPath;
	
	[self.delegate updateMetodoPago:indexPath.row + 1];
}

@end
