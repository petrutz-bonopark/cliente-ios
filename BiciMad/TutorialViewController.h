//
//  TutorialViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 26/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController

@property NSUInteger pageIndex;
@property (strong, nonatomic) IBOutlet UILabel *labelTutorial;
@property (weak, nonatomic) IBOutlet UIImageView *imagenFondo;

@property (strong, nonatomic) NSArray *tutorialPageTitles;
@property (strong, nonatomic) NSArray *tutorialPageImages;

@end
