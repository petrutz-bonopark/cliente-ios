//
//  TutorialViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 26/03/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "TutorialViewController.h"

@interface TutorialViewController ()

@end

@implementation TutorialViewController

@synthesize imagenFondo;
@synthesize labelTutorial;
@synthesize tutorialPageTitles;
@synthesize tutorialPageImages;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{

    tutorialPageImages = @[@"screen.png", @"screen1.png", @"screen2.png", @"screen3.png", @"screen4.png"];

    self.imagenFondo.image = [UIImage imageNamed:[self pageImages:(int) self.pageIndex]];
    self.labelTutorial.text = [self pageTitles:(int) self.pageIndex];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)pageTitles: (int) index
{
    static NSArray *_titles;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _titles = @[@"Bienvenido al servicio de bicicleta pública del Ayuntamiento de Madrid",
                    @"Obtén un descuento con la tarjeta de transporte público",
                    @"Navega por el mapa y encuentra la estación más cercana a tu destino",
                    @"Comprueba el tiempo que hace en madrid antes de salir",
                    @"Mira en que posición del ranking estás y consigue bonificaciones"];
    });
    return _titles[index];
}

- (NSString *)pageImages: (int) index
{
    static NSArray *_images;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _images = @[@"screen.png",
                    @"screen1.png",
                    @"screen2.png",
                    @"screen3.png",
                    @"screen4.png"];
    });
    return _images[index];
}

@end
