//
//  WebFormTPVViewController.h
//  BiciMad
//
//  Created by Miguel Asuar on 10/04/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComunObject.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "AltaUsuarioViewController.h"


@interface WebFormTPVViewController : UIViewController <UIWebViewDelegate, UIAlertViewDelegate>

@property (assign, nonatomic) NSInteger tipoOperacion;

@property (assign, nonatomic) NSInteger importePago;
@property (assign, nonatomic) NSString * nombreUsuario;
@property (assign, nonatomic) NSString * apellidoUsuario;
@property (assign, nonatomic) NSString * apellido2Usuario;
@property (assign, nonatomic) NSString * emailUsuario;
@property (assign, nonatomic) NSString * numeroTarjetaTransporte;

@property (strong, nonatomic) IBOutlet UIWebView *webView;
- (void)loadRequestFromString:(NSString*)urlString;
@end
