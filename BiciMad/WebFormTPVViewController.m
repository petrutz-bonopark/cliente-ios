//
//  WebFormTPVViewController.m
//  BiciMad
//
//  Created by Miguel Asuar on 10/04/14.
//  Copyright (c) 2014 Bonopark SL. All rights reserved.
//

#import "WebFormTPVViewController.h"

@interface WebFormTPVViewController ()

@end

@implementation WebFormTPVViewController

{

    MBProgressHUD *progressHUD;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.tipoOperacion == TIPO_PAGO_ALTA) {
        if ([PreferenciasUsuarios getBooleanInPreferences:PREFERENCIA_USUARIO_CON_TUTOR]) {
            [self loadAltaUsuarioConTutor];
        } else {
            [self loadAltaUsuario];
        }
        
    } else {
        NSString * merchantData = [NSString stringWithFormat:@"%@//%ld",
                        [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DNI],
                        (long)self.importePago];
        
        [self loadRequestFromString:[NSString stringWithFormat:@"%@%@?residente=%@&importe=%ld",BaseURLString, url_submint_form_recarga_TPV,  merchantData, (long)self.importePago]];
    }
}

- (void) loadAltaUsuario {

    NSString * merchantData = [NSString stringWithFormat:@"%@//%@//%@//%@//%@//%@//%@//%@//%@//%@//%@//%@//%@//%ld//%@//%@//%@//%@",
                               /* DNI */           [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DNI],
                               /* Nombre */        [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_NOMBRE],
                               /* Apellido1 */     [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO],
                               /* Apellido2 */     [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO2],
                               /* Idioma */        @"es",
                               /* Fecha nac */     [ComunObject formatDateFromString:
                                                    [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_FECHA_NAC]
                                                                           patternIN:@"dd/MM/yyyy" patternOUT:@"yyyyMMdd"],
                               
                               /* Movil */         [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_TELEFONO],
                               /* Email */         [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_EMAIL],
                               /* Direccion */     [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DIRECCION],
                               /* Municipio */     [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_MUNICIPIO],
                               /* Provincia */     [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_PROVINCIA],
                               /* Cod. postal */   [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_COD_POSTAL],
                               /* Nº tarj. consorc */ self.numeroTarjetaTransporte,
                               /* Canal */            (long)TIPO_ALTA_BBDD_MOVIL,
                               /* IDOPERADOR CRM */    @"0",
                               /* Tipo documento */    @"1",
                               /* Clave recogida */    [ComunObject generarCodigoRecogida:5],
                               /* Saldo incial */      @"0"];
    
    //merchantData = [merchantData stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    // Probar la acción cuando el pago es OK
    
    NSString *url = [NSString stringWithFormat:@"%@%@?residente=%@&importe=%ld", BaseURLString, url_submint_form_TPV, merchantData, (long)self.importePago];
    [self loadRequestFromString:url];
}

- (void) loadAltaUsuarioConTutor {
    NSString * merchantData = [NSString stringWithFormat:@"%@//%@//%@//%@//%@//%@//%@//%@//%@//%@//%@//%@//%@//%ld//%@//%@//%@//%@//%@//%@//%@//%@//%@",
                               /* DNI */           [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DNI],
                               /* Nombre */        [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_NOMBRE],
                               /* Apellido1 */     [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO],
                               /* Apellido2 */     [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO2],
                               /* Idioma */        @"es",
                               /* Fecha nac */     [ComunObject formatDateFromString:
                                                    [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_FECHA_NAC]
                                                                           patternIN:@"dd/MM/yyyy" patternOUT:@"yyyyMMdd"],
                               
                               /* Movil */         [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_TELEFONO],
                               /* Email */         [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_EMAIL],
                               /* Direccion */     [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DIRECCION],
                               /* Municipio */     [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_MUNICIPIO],
                               /* Provincia */     [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_PROVINCIA],
                               /* Cod. postal */   [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_COD_POSTAL],
                               /* Nº tarj. consorc */ self.numeroTarjetaTransporte,
                               /* Canal */            (long)TIPO_ALTA_BBDD_MOVIL,
                               /* IDOPERADOR CRM */    @"0",
                               /* Tipo documento */    @"1",
                               /* Clave recogida */    [ComunObject generarCodigoRecogida:5],
                               /* Saldo incial */      @"0",
                               /* DNI Tutor */      [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_DNI_TUTOR],
                               /* Tipo doc Tutor */ @"1",
                               /* Nombre tutor */   [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_NOMBRE_TUTOR],
                               /* Apell tutor */    [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO_TUTOR],
                               /* Apell2 tutor */   [PreferenciasUsuarios getStringInPreferences:PREFERENCIA_APELLIDO2_TUTOR]
                                                    ];
    
    //merchantData = [merchantData stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    // Probar la acción cuando el pago es OK
    NSString *url = [NSString stringWithFormat:@"%@%@?residente=%@&importe=%ld", BaseURLString, url_submint_form_TPV_tutor, merchantData, (long)self.importePago];
    [self loadRequestFromString:url];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadRequestFromString:(NSString*)urlString
{
    NSURL *url = [[NSURL alloc] initWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:urlRequest];
    self.webView.delegate = self;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if ([webView.request.URL.path rangeOfString:@"pago_ok"].location != NSNotFound) {
        if (!progressHUD.isHidden) {
            [progressHUD hide:YES];
        }
        
            double delayInSeconds = 1.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                NSLog(@"Pago recibido OK!");
            
                if (self.tipoOperacion == 2) {
                    // Cierro la ventana de la recarga de saldo
                    [self dismissViewControllerAnimated:true completion:nil];
                } else {
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Registro completo en BiciMAD" message:@"Acceda a la app con la contraseña que le hemos enviado al correo con el que se ha dado de alta" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
                    alert.alertViewStyle = UIAlertViewStyleDefault;
                    alert.tag = 1;
                    [alert show];
                }
   
            });
    }
    
    if (!progressHUD.isHidden) {
        [progressHUD hide:YES];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    progressHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    progressHUD.dimBackground = YES;
    progressHUD.labelText = @"Conectando con el servidor de pago";

    if ([request.URL.scheme isEqualToString:@"editar_datos"]) {
        [self dismissViewControllerAnimated:TRUE completion:Nil];
        if (!progressHUD.isHidden) {
            [progressHUD hide:YES];
        }
    }
    
    if (!progressHUD.isHidden) {
        [progressHUD hide:YES];
    }
    return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //NSLog(@"Entered: %@",[[alertView textFieldAtIndex:0] text]);
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            [self dismissViewControllerAnimated:NO completion:^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"callSegueAbrirSesion" object:self];
            }];
        }
    }
}

@end
