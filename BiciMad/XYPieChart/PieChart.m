//
//  PieChart.m
//  Try_PieChart
//
//  Created by Imran on 23/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PieChart.h"


@implementation PieChart

-(void)setVal1:(float)val1 setVal2:(float)val2
{
	self->m_val1=val1;
	self->m_val2=val2;

}


- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    // Drawing code
	float sum=self->m_val1 + self->m_val2;
	float mult=(360/sum);
	
	float startDeg=0;
	float endDeg=0;
	
	int x=100;
	int y=100;
	int r=60;
	
	CGContextRef ctx=UIGraphicsGetCurrentContext();
	//CGContextSetRGBStrokeColor(ctx, 1.0, 0.0, 1.0, 0.0);
	//CGContextSetLineWidth(ctx, 2.0);
	
	startDeg=0;
	endDeg=(self->m_val1 * mult);
	if(startDeg != endDeg)
	{
		CGContextSetRGBFillColor(ctx, 1.0, 0.0, 0.0, 1.0);
		CGContextMoveToPoint(ctx, x, y);
		CGContextAddArc(ctx, x, y, r, (startDeg)*M_PI/180.0, (endDeg)*M_PI/180.0, 0);
		CGContextClosePath(ctx);
		CGContextFillPath(ctx);
	
	
	}
	
	startDeg=endDeg;
	endDeg=endDeg + (self->m_val2 * mult);
	if(startDeg != endDeg)
	{
		CGContextSetRGBFillColor(ctx, 0.0, 1.0, 1.0, 1.0);
		CGContextMoveToPoint(ctx, x, y);
		CGContextAddArc(ctx, x, y, r, (startDeg)*M_PI/180.0, (endDeg)*M_PI/180.0, 0);
		CGContextClosePath(ctx);
		CGContextFillPath(ctx);
		
		
	}
	
}



@end
